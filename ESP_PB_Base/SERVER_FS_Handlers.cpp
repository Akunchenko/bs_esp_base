#include <FS.h>
#include <ESP8266WebServer.h>

ESP8266WebServer* _serverFS;
File fsUploadFile;
// ����� ������� ��� ������ � �������� ��������
String getContentType(String filename) {
    if (_serverFS->hasArg("download")) return "application/octet-stream";
    else if (filename.endsWith(".htm")) return "text/html";
    else if (filename.endsWith(".html")) return "text/html";
    else if (filename.endsWith(".css")) return "text/css";
    else if (filename.endsWith(".js")) return "application/javascript";
    else if (filename.endsWith(".png")) return "image/png";
    else if (filename.endsWith(".gif")) return "image/gif";
    else if (filename.endsWith(".jpg")) return "image/jpeg";
    else if (filename.endsWith(".ico")) return "image/x-icon";
    else if (filename.endsWith(".xml")) return "text/xml";
    else if (filename.endsWith(".pdf")) return "application/x-pdf";
    else if (filename.endsWith(".zip")) return "application/x-zip";
    else if (filename.endsWith(".gz")) return "application/x-gzip";
    else if (filename.endsWith(".wav")) return "audio/wav";
    return "text/plain";
}

bool handleFileRead(String path) {

    if (path.endsWith("/")) path += "index.htm";
    String contentType = getContentType(path);
    String pathWithGz = path + ".gz";
    if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) {
        if (SPIFFS.exists(pathWithGz))
            path += ".gz";
        File file = SPIFFS.open(path, "r");
        size_t sent = _serverFS->streamFile(file, contentType);
        file.close();
        return true;
    }
    return false;
}

void handleFileUpload() {
    if (_serverFS->uri() != "/edit") return;
    HTTPUpload& upload = _serverFS->upload();
    if (upload.status == UPLOAD_FILE_START) {
        String filename = upload.filename;
        if (!filename.startsWith("/")) filename = "/" + filename;
        fsUploadFile = SPIFFS.open(filename, "w");
        filename = String();
    }
    else if (upload.status == UPLOAD_FILE_WRITE) {
        //DBG_OUTPUT_PORT.print("handleFileUpload Data: "); DBG_OUTPUT_PORT.println(upload.currentSize);
        if (fsUploadFile)
            fsUploadFile.write(upload.buf, upload.currentSize);
    }
    else if (upload.status == UPLOAD_FILE_END) {
        if (fsUploadFile)
            fsUploadFile.close();
    }
}

void handleFileDelete() {
    if (_serverFS->args() == 0) return _serverFS->send(500, "text/plain", "BAD ARGS");
    String path = _serverFS->arg(0);
    if (path == "/")
        return _serverFS->send(500, "text/plain", "BAD PATH");
    if (!SPIFFS.exists(path))
        return _serverFS->send(404, "text/plain", "FileNotFound");
    SPIFFS.remove(path);
    _serverFS->send(200, "text/plain", "");
    path = String();
}
void handleFileCreate() {
    if (_serverFS->args() == 0)
        return _serverFS->send(500, "text/plain", "BAD ARGS");
    String path = _serverFS->arg(0);
    if (path == "/")
        return _serverFS->send(500, "text/plain", "BAD PATH");
    if (SPIFFS.exists(path))
        return _serverFS->send(500, "text/plain", "FILE EXISTS");
    File file = SPIFFS.open(path, "w");
    if (file)
        file.close();
    else
        return _serverFS->send(500, "text/plain", "CREATE FAILED");
    _serverFS->send(200, "text/plain", "");
    path = String();

}
void handleFileList() {

    if (!_serverFS->hasArg("dir")) {
        _serverFS->send(500, "text/plain", "BAD ARGS");
        return;
    }
    String path = _serverFS->arg("dir");
    Dir dir = SPIFFS.openDir(path);
    path = String();
    String output = "[";
    while (dir.next()) {
        File entry = dir.openFile("r");
        if (output != "[") output += ',';
        bool isDir = false;
        output += "{\"type\":\"";
        output += (isDir) ? "dir" : "file";
        output += "\",\"name\":\"";
        output += String(entry.name()).substring(1);
        output += "\"}";
        entry.close();
    }
    output += "]";

    _serverFS->send(200, "text/json", output);
}


void FS_init(ESP8266WebServer& _server) {
    _serverFS = &_server;
    //HTTP �������� ��� ������ � FFS
    //list directory
    _serverFS->on("/list", HTTP_GET, handleFileList);
    //�������� ��������� editor
    _server.on("/edit", HTTP_GET, []() {
        if (!handleFileRead("/edit.htm")) _serverFS->send(404, "text/plain", "FileNotFound");
        });
    //�������� �����
    _serverFS->on("/edit", HTTP_PUT, handleFileCreate);
    //�������� �����
    _serverFS->on("/edit", HTTP_DELETE, handleFileDelete);
    //first callback is called after the request has ended with all parsed arguments
    //second callback handles file uploads at that location
    _serverFS->on("/edit", HTTP_POST, []() {
        _serverFS->send(200, "text/plain", "");
        }, handleFileUpload);

    //called when the url is not defined here
    //use it to load content from SPIFFS
    _server.onNotFound([]() {
        if (!handleFileRead(_serverFS->uri()))
            _serverFS->send(404, "text/plain", "FileNotFound");

        });
}
