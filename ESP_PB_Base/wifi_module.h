// wifi_module.h

#ifndef _WIFI_MODULE_h
#define _WIFI_MODULE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include <ESP8266WiFi.h>
void InitWiFi();
void HandleWiFi();
bool WFM_GetPartalState();
void WFM_SetCustomIpAddres(IPAddress& ip_addr);
IPAddress WFM_GetCustomIpAddres();

void WFM_SetConnStateApMode(uint8_t _data);
uint8_t WFM_GetConnStateApMode();
#endif

