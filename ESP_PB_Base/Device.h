#pragma once
#include <Arduino.h>
class Device
{
public:
	Device();
	~Device();
	uint8_t getLoraAddress();
	uint8_t getId();
	uint8_t getBatteryLevel();
	uint32_t getMsgCount();
	String getMsg();

	void parceMsg();

private:
	uint8_t loraAddress;
	uint8_t id;
	uint8_t batteryLevel;
	uint32_t msgCount;
	String msg;
};