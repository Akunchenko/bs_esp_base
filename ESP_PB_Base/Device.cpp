#include "Device.h"
Device::Device()
{
	this->loraAddress = 0;
	this->id = 0;
	this->batteryLevel = 0;
	this->msg = "";
	this->msgCount = 0;
}

Device::~Device()
{
}
uint8_t Device::getLoraAddress() { return this->loraAddress; }
uint8_t Device::getId() { return this->id; }
uint8_t Device::getBatteryLevel() { return this->batteryLevel; }
uint32_t Device::getMsgCount() { return this->msgCount; }
String Device::getMsg() { return this->msg; }

void Device::parceMsg() {};