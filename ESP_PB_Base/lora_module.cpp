// 
// 
// 

#include "lora_module.h"
#include "esp.pb.h"

uint16_t msgCount = 0;            // count of outgoing messages
const byte localAddress = 0xFF;     // address of this device
byte destination = 0xBB;      // destination to send to
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends
const uint8_t csPin = 15, resetPin = 0, irqPin = 2;

uint8_t g_sender;

uint8_t loraClient;

uint32_t time_asd;
uint32_t diffTime;
uint32_t err;

typedef struct {
	uint8_t ownAddress;
	uint8_t targetAddress;
	uint32_t uuid;
}AckInfo;

typedef struct {
	uint16_t recipient;
	SlavesTypeDevice loraClient;
	uint16_t msgId;
	uint8_t batLevel;
	uint8_t uuid[4];
	uint8_t rssi;
}LoraReceiveInfo;


void InitLora() {
	Serial.println("LoRa Duplex");
	// override the default CS, reset, and IRQ pins (optional)
	LoRa.setPins(csPin, resetPin, irqPin);// set CS, reset, IRQ pin

	if (!LoRa.begin(433E6)) {             // initialize ratio at 433MHz
		Serial.println("LoRa init failed. Check your connections.");
		while (true);                       // if failed, do nothing
	}
	LoRa.setSpreadingFactor(7);
	LoRa.setSignalBandwidth(125E3);
	LoRa.setCodingRate4(0);
	LoRa.setTxPower(20);
	Serial.println("LoRa init succeeded.");
}

void sendMessage(String outgoing) {
	LoRa.beginPacket();                   // start packet
	LoRa.write(destination);              // add destination address
	LoRa.write(localAddress);             // add sender address
	LoRa.write(msgCount);                 // add message ID
	LoRa.write(outgoing.length());        // add payload length
	LoRa.print(outgoing);                 // add payload
	LoRa.endPacket();                     // finish packet and send it
	msgCount++;                           // increment message ID
}

void sendACK(uint8_t _target, uint32_t _buf) {
	AckInfo ack;
	ack.ownAddress = localAddress;
	ack.targetAddress = _target;
	ack.uuid = _buf;
	LoRa.beginPacket();                   // start packet
	LoRa.write(ack.targetAddress);             // add sender address
	LoRa.write(ack.ownAddress);              // add destination address
	LoRa.write((uint8_t*)&ack.uuid, 4);             // add payload
	LoRa.endPacket();                     // finish packet and send it
}

void ParceTowelInfo(LoraReceiveInfo* _data) {
	SlaveInfoTowel slaveTowel = { 0 };

	//slaveTowel.batteryLevel = _data->batLevel;
	slaveTowel.batteryLevel = _data->rssi;
	slaveTowel.uuid.len = sizeof(_data->uuid);
	slaveTowel.uuid.ptr = (char*)_data->uuid;
	
	SendSlaveInfoTowel(&slaveTowel);
}

void ParceButtonInfo(LoraReceiveInfo *_data) {
	SlaveInfoButton slaveButton = { 0 };

	slaveButton.batteryLevel = _data->batLevel;
	slaveButton.uuid.len = sizeof(_data->uuid);
	slaveButton.uuid.ptr = (char*)_data->uuid;

	SendSlaveInfoButton(&slaveButton);
}

void DumpLoraReceive(LoraReceiveInfo data) {
	Serial.println("Received from: 0x" + String(data.loraClient, HEX));
	Serial.println("Message ID: " + String(data.msgId));
	Serial.println("Battery level: " + String(data.batLevel));
	/*for (uint8_t i = 0; i < 4; i++)
	{
		Serial.println("Serial ID[" + String(i) + "]:" + String(data.uuid[i], HEX));
	}*/

	Serial.println("RSSI: -" + String(data.rssi));
	Serial.println("Snr: " + String(LoRa.packetSnr()));
	Serial.println("-----------------------------");
}

void onReceive(int packetSize) {
	if (packetSize == 0) return;          // if there's no packet, return
	diffTime = millis() - time_asd;
	time_asd = millis();
	
	Serial.print("Diff time for msgs: ");
	Serial.println(diffTime);

	LoraReceiveInfo rData;

	rData.recipient = LoRa.read();
	if(rData.recipient == localAddress){
		rData.loraClient = (SlavesTypeDevice)LoRa.read();
		rData.msgId = (LoRa.read() << 8) | LoRa.read();
		rData.batLevel = LoRa.read();
		//rData.uuid = (LoRa.read() << 24) | (LoRa.read() << 16) | (LoRa.read() << 8) | LoRa.read();
		for (uint8_t i = 0; i < 4; i++)
		{
			rData.uuid[i] = LoRa.read();
		}
		rData.rssi = abs(LoRa.packetRssi());
		switch (rData.loraClient)
		{
			case SlaveTypeButton:
				ParceButtonInfo(&rData);
				break;
			case SlaveTypeTowel:
				ParceTowelInfo(&rData);
				break;
			case SlaveTypeScoreBoard:
				break;
			case SlaveTypeSpot:
				break;
			case SlaveTypeRemControl:
				break;

			default:
				break;
		}
	}
	DumpLoraReceive(rData);
	/*// read packet header bytes:
	uint16_t recipient = LoRa.read();          // recipient address
	SlavesTypeDevice loraClient = (SlavesTypeDevice)LoRa.read();            // sender address
	// if the recipient isn't this device or broadcast,
	if (recipient != localAddress) {
		Serial.println("This message is not for me.");
		return;                             // skip rest of function
	}
	else
	{
		Serial.println("Received from: 0x" + String(loraClient, HEX));
		byte msgId_2 = LoRa.read();     // incoming msg ID part2
		byte msgId_1 = LoRa.read();		// incoming msg ID part1
		uint16_t incomingMsgId = (msgId_1 << 8) | msgId_2;     // incoming msg ID
		
		Serial.println("Message ID: " + String(incomingMsgId));
		
		byte batLevel = LoRa.read();
		Serial.println("Battery level: " + String(batLevel));

		uint8_t numb[4] = { 0 };
		for (uint8_t i = 0; i < 4; i++)
		{
			numb[i] = LoRa.read();
			Serial.println("Serial ID[" + String(i) + "]:" + String(numb[i], HEX));
		}
		
		switch (loraClient)
		{
			case SlaveTypeButton:

				break;
			case SlaveTypeTowel:
				//ParceTowelInfo();
				break;
			case SlaveTypeScoreBoard:
				break;
			case SlaveTypeSpot:
				break;
			case SlaveTypeRemControl:
				break;

			default:
				break;
		}
	
	
	
		Serial.println("RSSI: " + String(LoRa.packetRssi()));
		Serial.println("Snr: " + String(LoRa.packetSnr()));
		Serial.println("Error " + String(err));
		Serial.println();
		
	}
	*/
	
}
uint32_t GetLoRaClientAdress() {
	return g_sender;
}
void SetLoRaClientAdress(uint32_t _sender) {
	g_sender = _sender;
}
