﻿/*
 Name:		ESP_PB_Base.ino
 Created:	16.09.2019 11:05:16
 Author:	Алексей
*/

// the setup function runs once when you press reset or power the board



#include <Adafruit_NeoPixel.h>
#include <Adafruit_MCP23017.h>
#include <dummy.h>
//#include <ESP8266FtpServer.h>
#include <WiFiManager.h>

#include <SPI.h>
#include "logic.h"
#include <FS.h>

MyLogic logic;

void setup() {
	Serial.begin(115200, SERIAL_8N1, SERIAL_TX_ONLY);                   // initialize serial
	//Serial.setDebugOutput(true);
	MyLogic();	
}

// the loop function runs over and over again until power down or reset
void loop() {
	
	logic.Handler();
   
}


