// 
// 
// 

#include "wifi_module.h"

#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <WiFiManager.h>  
#include "SERVER_Handlers.h"

#include "Indication.h"
#include "Lan_proto/udp_io.h"
#include "Lan_proto/callbacs.h"
#include "Lan_proto/esp_server_handlers.h"
#include "Lan_proto/task_sheduler.h"

#include <Lan_proto\server_discovery.h>
extern "C" {
#include<user_interface.h>
}

WiFiManager wm;
WiFiEventHandler stationConnectedHandler;
WiFiEventHandler stationDisconnectedHandler;
WiFiEventHandler stationStationModeGotIP;

IPAddress _ip = IPAddress(10, 0, 0, 4);
unsigned int localPort = 8888;      // ����, � �������� ��������� UDP ������
const int NTP_PACKET_SIZE = 48; // ������ ������ ��� ������������ �������
char packetBuffer[NTP_PACKET_SIZE]; //����� �������� �������

bool portalRunning = false;
bool startAP = false; // start AP and webserver if true, else start only 
bool broadcastInNet = false;
IPAddress ipClient;
String ssid = "HUB_" + String(WiFi.softAPmacAddress());
//String ssid = "PB_BASE_" + String(ESP.getChipId());

unsigned int  timeout = 120; // seconds to run for
unsigned int  startTime = millis();
uint8_t connectStateApMode = 0;

void client_status();
bool WFM_GetPartalState() {
	return portalRunning;
}
void WFM_SetConnStateApMode(uint8_t _data) {
	connectStateApMode = _data;
}
uint8_t WFM_GetConnStateApMode() {
	return connectStateApMode;
}
void WFM_SetCustomIpAddres(IPAddress& ip_addr) {
	ipClient = ip_addr;
}
IPAddress WFM_GetCustomIpAddres() {
	return ipClient;
}
String macToString(const unsigned char* mac) {
	char buf[20];
	snprintf(buf, sizeof(buf), "%02x:%02x:%02x:%02x:%02x:%02x",
		mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	return String(buf);
}
void onStationConnected(const WiFiEventSoftAPModeStationConnected& evt) {
	Serial.print("Station connected: ");
	Serial.println(macToString(evt.mac));
}

void onStationDisconnected(const WiFiEventSoftAPModeStationDisconnected& evt) {
	Serial.print("Station disconnected: ");
	Serial.println(macToString(evt.mac));
}
void wifi_handle_event_cb(System_Event_t* evt) {
	//EspConectionState msg = { 0 };
	//char Status[64];
	char serial[] = "12345";
	string serial_str;
	serial_str.ptr = serial;
	serial_str.len = strlen(serial);

	switch (evt->event) {
	case EVENT_STAMODE_CONNECTED:
		Serial.printf("WiFI Callback: EVENT_STAMODE_CONNECTED\n");
		indication.ConnectingToServer();
		//LogBuff_AddEvent(WIFI_EVENT, EVENT_STAMODE_CONNECTED);
		break;

	case EVENT_STAMODE_GOT_IP:
		Serial.printf("WiFI Callback: EVENT_STAMODE_GOT_IP\n");
		//LogBuff_AddEvent(WIFI_EVENT, EVENT_STAMODE_GOT_IP);
		connectStateApMode = 1;
		serv_dis::askServersToAnnounce(1, serial_str);
		break;


	case EVENT_STAMODE_DISCONNECTED:
		Serial.printf("WiFI Callback: EVENT_STAMODE_DISCONNECTED\n");
		//LogBuff_AddEvent(WIFI_EVENT, EVENT_STAMODE_DISCONNECTED);
		//case EVENT_STAMODE_DHCP_TIMEOUT:
		/*
		msg.wifi_status = WL_IDLE_STATUS;
		msg.tcp_status = udp_io::status();
		if (!SendEspConectionState(&msg))
			SetRuntimeError(RTDB_SEND_CONN_STATE_FAILED);
		  */
		  /*msg.wifi_status = WL_NO_SSID_AVAIL;
		  msg.tcp_status = udp_io::status();
		  last_wifi_status = WL_NO_SSID_AVAIL;
		  if (!SendEspConectionState(&msg))
			  SetRuntimeError(RTDB_SEND_CONN_STATE_FAILED);

		  main_ssid_found = false;*/
		break;

	case EVENT_STAMODE_AUTHMODE_CHANGE:
		//strcpy(Status, "EVENT_STAMODE_AUTHMODE_CHANGE");
		Serial.printf("WiFI Callback: EVENT_STAMODE_AUTHMODE_CHANGE\n");
		//LogBuff_AddEvent(WIFI_EVENT, EVENT_STAMODE_AUTHMODE_CHANGE);
		break;

	case EVENT_STAMODE_DHCP_TIMEOUT:
		Serial.printf("WiFI Callback: EVENT_STAMODE_DHCP_TIMEOUT\n");
		//LogBuff_AddEvent(WIFI_EVENT, EVENT_STAMODE_DHCP_TIMEOUT);
		break;
	case EVENT_SOFTAPMODE_STACONNECTED:
		Serial.printf("WiFI Callback: EVENT_SOFTAPMODE_STACONNECTED\n");
		
		
		break;

	}

}

void InitWiFi() {

	WiFi.mode(WIFI_AP);
	wm.setConfigPortalBlocking(false);
	
	IPAddress _gw = IPAddress(10, 0, 0, 1);
	IPAddress _sn = IPAddress(255, 255, 255, 0);
	
	wm.resetSettings();

	wifi_set_event_handler_cb(wifi_handle_event_cb);
	esp_srv::init();
	udp_io::init(OnConnectionEstablished, OnConnectionClosed, OnFullCmdReceived);
	wm.setAPStaticIPConfig(_ip, _ip, _sn);
	if (wm.autoConnect(ssid.c_str())) {
		broadcastInNet = true;
	}
	else
	{
		indication.PortalIsActive();
		portalRunning = true;
	}
	Serial.printf("Web server started, open %s in a web browser\n", WiFi.localIP().toString().c_str());
	
	//udp_io::EspConnectToAp("HomeProductionTP", "");
	
}

void client_status() {
	if (connectStateApMode == 0) {
		unsigned char number_client;
		struct station_info* stat_info;

		struct ip4_addr* IPaddress;
		IPAddress address;
		int i = 1;

		number_client = wifi_softap_get_station_num();
		stat_info = wifi_softap_get_station_info();

		/*Serial.print(" Total Connected Clients are = ");
			Serial.println(number_client);*/
		/*if (stat_info == NULL) {
			connectState = 0;
		}*/
	
		while (stat_info != NULL) {

			IPaddress = &stat_info->ip;
			address = IPaddress->addr;

			Serial.print("client= ");

			Serial.print(i);
			Serial.print(" IP adress is = ");
			Serial.println((address));
			string name;
			name.ptr = "ASADA";

			char serial[] = "12345";
			string serial_str;
			serial_str.ptr = serial;
			serial_str.len = strlen(serial);
			serv_dis::askServersToAnnounce(1, serial_str, address);
			WFM_SetCustomIpAddres(address);
			Serial.println("Try connect to server in AP\n\r");
			connectStateApMode = 1;
			stat_info = STAILQ_NEXT(stat_info, next);
			i++;
			Serial.println();
		}
	}
}
void HandleWiFi() {
	
	if (wm.process()) {
		broadcastInNet = true;
	}
	SendLikePing();
	client_status();
	udp_io::update();
	TaskSheduler_Update();
	delay(1);
}
