#pragma once
#include "Blinking_led/src/Led_library.h"
class Indication
{
public:
	Indication();
	~Indication();
	void Handler(void);
	enum wsLeds {
		WS_POWER_LED = 0,
		WS_STATUS_LED = 1,
		WS_CHARGE_LED = 2,
	};
	enum WS_COLORS {
		WS_RED_COLOR = 0xff0000,
		WS_PINK_COLOR = 0xff00a5,
		WS_GREEN_COLOR = 0x00ff00,
		WS_YELLOW_COLOR = 0xffff00,
		WS_ORANGE_COLOR = 0xff8000,
		WS_BLUE_COLOR = 0x0000ff,
		WS_LIGHT_BLUE = 0x0bffff,
		WS_WHITE_COLOR = 0xffffff,
		WS_BLACK_COLOR = 0,
	};
	void SetChargeWsColor(WS_COLORS _color);

	void rainbow(int wait);

	void Blinking();

	void DisconnectFromServer();

	void ConnectingToServer();

	void ConnectedToServer();

	void PortalIsActive();

	void StopLeds();

	void powerOnOFF();
	
private:
	Led power;
	Led charge;
	Led states;
	Led allLeds;
	Led buzz;
	//AudioBuzzer buz;
};
extern Indication indication;

