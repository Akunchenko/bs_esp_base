#include "Indication.h"

#include <Adafruit_NeoPixel.h>
#include "Periphery.h"
Indication indication = Indication();

#define WS_LED_PIN 3
#define LED_COUNT 3
Adafruit_NeoPixel strip(LED_COUNT, WS_LED_PIN, NEO_GRB + NEO_KHZ800);

uint32_t rainbowTimer;
long firstPixelHue = 0;

uint32_t colorAllLeds;
uint32_t colorPowerLed; 
uint32_t colorStatusLed;
uint32_t colorChargeLed;

const int continius[] = {
    1
};
const int tempContinius[] = {
    1000
};
DescriptLedBlinking indContinius = { (int*)continius, (int*)tempContinius, sizeof(continius) / sizeof(int) };

const int blinking[] = {
    1,0
};
const int tempBlinking[] = {
    500,500
};
DescriptLedBlinking indBlinking = { (int*)blinking, (int*)tempBlinking, sizeof(blinking) / sizeof(int) };

uint32_t GetTime(void) {
    return millis();
}

void TurnOnPowerLed(void)
{
    int8_t R = (colorPowerLed >> 16);
    uint8_t G = (colorPowerLed >> 8);
    uint8_t B = colorPowerLed;
    strip.setPixelColor(Indication::WS_POWER_LED, strip.Color(R, G, B));
    strip.show();
}

void TurnOffPowerLed(void)
{
    strip.setPixelColor(Indication::WS_POWER_LED, 0, 0, 0);
    strip.show();
}

void PowerLedIndicationIsOver(DescriptLedBlinking* _file)
{
}
void TurnOnAllLeds(void)
{
    int8_t R = (colorAllLeds >> 16);
    uint8_t G = (colorAllLeds >> 8);
    uint8_t B = colorAllLeds;
    for (uint8_t i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, strip.Color(R, G, B));
    }
    strip.show();
}

void TurnOffAllLeds(void)
{
    for (uint8_t i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, 0, 0, 0);
    }
    strip.show();
}

void AllLedsIndicationIsOver(DescriptLedBlinking* _file)
{
   /* if (_file->duration == indBlinking.duration) {
        switch (colorAllLeds)
        {
        case Indication::WS_GREEN_COLOR:
            colorAllLeds = Indication::WS_BLUE_COLOR;
            break;
        case Indication::WS_BLUE_COLOR:
            colorAllLeds = Indication::WS_RED_COLOR;
            break;
        case Indication::WS_RED_COLOR:
            colorAllLeds = Indication::WS_GREEN_COLOR;
            break;
        default:
            break;
        }
        indication.Blinking();
    }*/
}

void TurnOnBuzzer(void)
{
    outward.SetBuzzerState(HIGH);
}

void TurnOffBuzzer(void)
{
    outward.SetBuzzerState(LOW);
}

void BuzzerIndicationIsOver(DescriptLedBlinking* _file) 
{

}

Indication::Indication()
{
	strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
	strip.setBrightness(100); // Set BRIGHTNESS to about 1/5 (max = 255)
	for (uint8_t i = 0; i < strip.numPixels(); i++) {
		strip.setPixelColor(i, 0, 0, 0);
	}
	strip.show();            // Turn OFF all pixels ASAP
    this->power.descr.GetTimeInms = GetTime;
    this->power.descr.TurnOnLed = TurnOnPowerLed;
    this->power.descr.TurnOffLed = TurnOffPowerLed;
    this->power.descr.IndicationIsOver = PowerLedIndicationIsOver;

    this->allLeds.descr.GetTimeInms = GetTime;
    this->allLeds.descr.TurnOnLed = TurnOnAllLeds;
    this->allLeds.descr.TurnOffLed = TurnOffAllLeds;
    this->allLeds.descr.IndicationIsOver = AllLedsIndicationIsOver;

    this->buzz.descr.GetTimeInms = GetTime;
    this->buzz.descr.TurnOnLed = TurnOnBuzzer;
    this->buzz.descr.TurnOffLed = TurnOffBuzzer;
    this->buzz.descr.IndicationIsOver = BuzzerIndicationIsOver;


    LED_Init(&this->power, &this->power.descr);
    LED_Init(&this->allLeds, &this->allLeds.descr);
    LED_Init(&this->buzz, &this->buzz.descr);

    colorAllLeds = WS_ORANGE_COLOR;
    LED_SetTask(&this->allLeds, &indContinius, LED_MODE_CONTINUOUS);
    //this->powerOnOFF();
}

Indication::~Indication(){}
void Indication::Handler(void)
{
    LED_Run(&this->allLeds);  
    LED_Run(&this->buzz);
}


void Indication::SetChargeWsColor(WS_COLORS _color) {
	uint8_t R = (_color >> 16);
	uint8_t G = (_color >> 8);
	uint8_t B = _color;
	strip.setPixelColor(WS_CHARGE_LED, strip.Color(R, G, B));
	strip.show();
}

void Indication::rainbow(int wait) {
    if (millis() >= rainbowTimer)
    {
        if (firstPixelHue < 5 * 65536) {
            firstPixelHue += 256;
        }
        else
        {
            firstPixelHue = 0;
        }
        rainbowTimer = millis() + wait;
    }
    for (int i = 0; i < strip.numPixels() - 1; i++) { // For each pixel in strip...
        int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
        strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents
}

void Indication::Blinking() {
    
    LED_SetTask(&this->allLeds, &indBlinking, LED_MODE_DISCONTINUOUS);
}

void Indication::DisconnectFromServer() {
    colorAllLeds = WS_ORANGE_COLOR;
    LED_SetTask(&this->allLeds, &indBlinking, LED_MODE_CONTINUOUS);
}

void Indication::ConnectingToServer() {
    colorAllLeds = WS_ORANGE_COLOR;
    LED_SetTask(&this->allLeds, &indBlinking, LED_MODE_CONTINUOUS);
}

void Indication::ConnectedToServer() {
    colorAllLeds = WS_GREEN_COLOR;
    LED_SetTask(&this->allLeds, &indConstStateOn, LED_MODE_CONTINUOUS);
}

void Indication::PortalIsActive() {
    colorAllLeds = WS_BLUE_COLOR;
    LED_SetTask(&this->allLeds, &indBlinking, LED_MODE_CONTINUOUS);
}
void Indication::StopLeds() {
    LED_ClearTask(&this->allLeds);
}
void Indication::powerOnOFF() {
    LED_SetTask(&this->buzz, &indBlinking, LED_MODE_DISCONTINUOUS);

}