#ifndef POWER_CONTROL_H_
#define POWER_CONTROL_H_
#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include "stddef.h"
#include <stdbool.h>

typedef enum{
	PWC_SET_ENABLE			=				0x01U,
	PWC_SET_DISABLE			=				0x02U
}SetPowerControl;

typedef enum{
	PWC_STATE_RELEASED				=					0x00U,
	PWC_STATE_PRESSED					=					0x01U
}PWC_StatePin;

typedef enum{
	PWC_TYPE_BUT_WITHOUT_FIX,
	PWC_TYPE_BUT_WITH_FIX
}PWC_TypeButton;

typedef struct{
	uint32_t timeout_enable;
	uint32_t timeout_disable;
	uint32_t timeout_error;
	bool resolution_disable;
}PowerControlConfig;

typedef struct{
	PowerControlConfig config;
	PWC_TypeButton type_button;
	bool (*set_power)(SetPowerControl set);
	PWC_StatePin (*get_state_button)(void);
	
	void (*enable_clbk)(void);
	void (*wait_enable_clbk)(void);
	void (*disable_clbk)(void);
}PowerControlContex;



void PWC_InitPowerControl(PowerControlContex *_cnt);

void PWC_CheckDisable(uint32_t (*_get_time_ms)(void));
void PWC_CheckEnable(uint32_t (*_get_time_ms)(void));
void PWC_CheckErrorDisable(uint32_t (*_get_time_ms)(void));
void PWC_SetPowerDisable(void);
void PWC_SetPowerEnable(void);

void PWC_ClearEvent(void);
void PWC_SetResolDisable(bool _resol);
void PWC_SetCongig(PowerControlConfig *_config);
void PWC_SetTimeoutDisable(uint32_t _timeout);
#ifdef __cplusplus
}
#endif
#endif
