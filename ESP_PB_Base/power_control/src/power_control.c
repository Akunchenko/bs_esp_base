
#include "power_control.h"


bool (*set_power)(SetPowerControl set);
uint32_t timeout_enable;
uint32_t timeout_disable;
uint32_t timeout_error;
PWC_StatePin (*get_state_button)(void);
bool resolution_disable;
PWC_TypeButton type_button;

void (*enable_clbk)(void);
void (*disable_clbk)(void);
void (*wait_enable_clbk)(void);
uint32_t time_pressing= 0;


void PWC_InitPowerControl(PowerControlContex *_cnt){
	set_power 					= _cnt->set_power;
	timeout_enable 			= _cnt->config.timeout_enable; 
	timeout_disable			=	_cnt->config.timeout_disable;
	timeout_error				=	_cnt->config.timeout_error;
	get_state_button		=	_cnt->get_state_button;
	resolution_disable	=	_cnt->config.resolution_disable;
	enable_clbk					=	_cnt->enable_clbk;
	disable_clbk				=	_cnt->disable_clbk;
	wait_enable_clbk		=	_cnt->wait_enable_clbk;
	type_button					=	_cnt->type_button;
}

void PowerDisable(void){
	while(!set_power(PWC_SET_DISABLE));
	while(true);
}

void PWC_SetPowerDisable(void){
	if(disable_clbk != NULL)
		disable_clbk();

	PowerDisable();
}

void PWC_SetPowerEnable(void){
	set_power(PWC_SET_ENABLE);

	if(enable_clbk != NULL)
		enable_clbk();
}

bool IsEnablePower(uint32_t (*_get_time_ms)(void)){
	uint32_t time_start = _get_time_ms();
	do{

		if(wait_enable_clbk != NULL)
			wait_enable_clbk();

		if((time_start + timeout_enable) <= _get_time_ms())
			return true;

	}while(get_state_button() == PWC_STATE_PRESSED);
	return false;
}



void PWC_CheckEnable(uint32_t (*_get_time_ms)(void)){	
	IsEnablePower(_get_time_ms) ? PWC_SetPowerEnable() : PowerDisable();
}



void PWC_CheckDisable(uint32_t (*_get_time_ms)(void)){
		PWC_StatePin state_but = get_state_button();
	if( (state_but == PWC_STATE_PRESSED 	&& 	type_button == PWC_TYPE_BUT_WITHOUT_FIX) ||
			(state_but == PWC_STATE_RELEASED &&	type_button == PWC_TYPE_BUT_WITH_FIX)     ){
		if((time_pressing + timeout_disable) <= _get_time_ms()	&&
				time_pressing != 0 &&	resolution_disable)
			PWC_SetPowerDisable();		
	}else
		time_pressing = _get_time_ms();
}

void PWC_CheckErrorDisable(uint32_t (*_get_time_ms)(void)){
	PWC_StatePin state_but = get_state_button();
	if(	((state_but == PWC_STATE_PRESSED 	&& 	type_button == PWC_TYPE_BUT_WITHOUT_FIX) 				||
			(state_but == PWC_STATE_RELEASED &&	type_button == PWC_TYPE_BUT_WITH_FIX))						&&
			(time_pressing + timeout_disable) <= _get_time_ms()	&&
			time_pressing != 0 &&	resolution_disable){

		PowerDisable();		

	}else
		time_pressing = _get_time_ms();
}

void PWC_SetResolDisable(bool _resol){
	resolution_disable = _resol;
}

void PWC_SetCongig(PowerControlConfig *_config){
	timeout_enable 			= _config->timeout_enable; 
	timeout_disable			=	_config->timeout_disable;
	resolution_disable	=	_config->resolution_disable;
	timeout_error				=	_config->timeout_error;
}
void PWC_ClearEvent(void){
	time_pressing = 0;
}
void PWC_SetTimeoutDisable(uint32_t _timeout){
	timeout_disable = _timeout;
}

