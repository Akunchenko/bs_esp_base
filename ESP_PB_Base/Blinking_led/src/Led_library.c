#include "Led_library.h"
#include "blinking_led.c"

void LED_Init(Led *_led, struct DescriptLed *_descr)
{
	memcpy(&_led->descr, _descr, sizeof(struct DescriptLed));
	_led->stateBlinking = LED_READY;
}
bool LED_SetTask(Led *_led, DescriptLedBlinking *file, WorkModeLed _mode)
{
	memcpy(&_led->file, file,sizeof(DescriptLedBlinking));
	_led->stateBlinking = LED_BLINKING;
	_led->numberStateInFile = 0;
	_led->mode = _mode;
	return true;
}
bool LED_SetOn(Led *_led, uint32_t timeout)
{
	bool res;
	if	(_led->stateBlinking != LED_READY){
		res = false;
	} else {	
		_led->timeChangeState = _led->descr.GetTimeInms() + timeout;
		_led->file.sizeBuf = 0;
		_led->descr.TurnOnLed();	
		_led->stateBlinking = LED_BLINKING;
		res = true;
	}
		return res;
}
void LED_Run(Led *_led)
{
	if(_led->stateBlinking == LED_BLINKING && _led->timeChangeState < _led->descr.GetTimeInms()){
		if(_led->numberStateInFile < (_led->file.sizeBuf)){
		
			if(_led->numberStateInFile == 0){
				_led->descr.TurnOffLed();
			}
			if(_led->file.state[_led->numberStateInFile] !=0){
				_led->descr.TurnOnLed();
			}else	{
				_led->descr.TurnOffLed();	
				}
			_led->timeChangeState = _led->descr.GetTimeInms() + (_led->file.duration[_led->numberStateInFile]);
			_led->numberStateInFile++;
		}
		else {
			_led->numberStateInFile = 0;
			if(_led->mode == LED_MODE_DISCONTINUOUS){
				_led->descr.TurnOffLed();			
				_led->stateBlinking = LED_READY;
				if(_led->descr.IndicationIsOver != NULL){
					_led->descr.IndicationIsOver(&_led->file);
				}
			}
		}
	}
}
void LED_ClearTask(Led *_led)
{
	if(_led->stateBlinking != LED_RESET){
		_led->numberStateInFile = 0;
		_led->stateBlinking = LED_READY;
		_led->descr.TurnOffLed();
		_led->timeChangeState = 0;
	}
}
