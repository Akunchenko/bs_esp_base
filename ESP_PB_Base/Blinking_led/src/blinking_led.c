#include "Led_library.h"
const int blinkVibroInd[] = {
	1, 0, 1, 0,
	1, 0, 1, 0,
};
const int tempVibroInd[] = {
	300, 300, 300, 300,
	300, 300, 300, 300,
};

DescriptLedBlinking indModeVibro = {(int*)blinkVibroInd, (int*)tempVibroInd, sizeof(blinkVibroInd)/sizeof(int)};


/*Snakes indication begin*/
const int blinkSnakesGreen[] = {
	1, 0,
};
const int tempSnakesGreen[] = {
	150, 750,
};

DescriptLedBlinking indSnakesGreen = {(int*)blinkSnakesGreen, (int*)tempSnakesGreen, sizeof(blinkSnakesGreen)/sizeof(int)};



const int blinkSnakesYellow[] = {
	0, 1, 0, 1,
};
const int tempSnakesYellow[] = {
	150, 150, 450, 150,
};

DescriptLedBlinking indSnakesYellow = {(int*)blinkSnakesYellow, (int*)tempSnakesYellow, sizeof(blinkSnakesYellow)/sizeof(int)};



const int blinkSnakesOrange[] = {
	0, 1, 0, 1, 0
};
const int tempSnakesOrange[] = {
	300, 150, 150, 150, 150
};

DescriptLedBlinking indSnakesOrange = {(int*)blinkSnakesOrange, (int*)tempSnakesOrange, sizeof(blinkSnakesOrange)/sizeof(int)};


const int blinkSnakesRed[] = {
	0, 1, 0,
};
const int tempSnakesRed[] = {
	450, 150, 300,
};

DescriptLedBlinking indSnakesRed = {(int*)blinkSnakesRed, (int*)tempSnakesRed, sizeof(blinkSnakesRed)/sizeof(int)};
/*Snakes indication end*/


/*battery levels indication begin*/
const int blinkMiddleLevelBat[] = {
	1,0,
};
const int tempMiddleLevelBat[] = {
	1000, 1000,
};
DescriptLedBlinking indMiddleLevelBat = {(int*)blinkMiddleLevelBat, (int*)tempMiddleLevelBat, sizeof(blinkMiddleLevelBat)/sizeof(int)};



const int blinkCriticalLowLevelBat[] = {
	1,0,1,0,1,0,1,0,1,0,
	1,0,1,0,1,0,1,0,1,0,
};
const int tempCriticalLowLevelBat[] = {
	250, 250, 250, 250, 250, 250, 250, 250,250, 250,
	250, 250, 250, 250, 250, 250, 250, 250,250, 250,
};
DescriptLedBlinking indCriticalLowLevelBat = {(int*)blinkCriticalLowLevelBat, (int*)tempCriticalLowLevelBat, sizeof(blinkCriticalLowLevelBat)/sizeof(int)};



const int blinkConstStateOn[] = {
	1
};
const int tempConstStateOn[] = {
	1000
};
DescriptLedBlinking indConstStateOn = {(int*)blinkConstStateOn, (int*)tempConstStateOn, sizeof(blinkConstStateOn)/sizeof(int)};


/*battery levels indication end*/


/*Indication about errors TM62 device begin*/

const int blinkErrorActivationTM62[] = {
	1,0,1,0,1,0,1,0,1,0,
	1,0,1,0,1,0,1,0,1,0,
};
const int tempErrorActivationTM62[] = {
	200, 100, 200, 1000, 200, 100, 200, 1000,200, 100,
	200, 1000, 200, 100, 200, 1000, 200, 100,200, 1000,
};
DescriptLedBlinking indErrorActivationTM62 = {(int*)blinkErrorActivationTM62, (int*)tempErrorActivationTM62, sizeof(blinkErrorActivationTM62)/sizeof(int)};



const int blinkErrorDeactivationTM62[] = {
	0,1,0,1,0,1,0,1,0,1,
	0,1,0,1,0,1,0,1,0,1,
};
const int tempErrorDeactivationTM62[] = {
	200, 100, 200, 1000, 200, 100, 200, 1000,200, 100,
	200, 1000, 200, 100, 200, 1000, 200, 100,200, 1000,
};
DescriptLedBlinking indErrorDeactivationTM62 = {(int*)blinkErrorDeactivationTM62, (int*)tempErrorDeactivationTM62, sizeof(blinkErrorDeactivationTM62)/sizeof(int)};


const int blinkErrorExplosionTM62[] = {
	0,1,0,1,0,1,0,1,0,1,
	0,1,0,1,0,1,0,1,0,1,
};
const int tempErrorExplosionTM62[] = {
	100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
	100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
};
DescriptLedBlinking indErrorExplosionTM62 = {(int*)blinkErrorExplosionTM62, (int*)tempErrorExplosionTM62, sizeof(blinkErrorExplosionTM62)/sizeof(int)};



/*Indication about errors TM62 device end*/

const int blinkHitting[] = {
	1,0,1,0,1,0,1,0,1,0
};
const int tempHitting[] = {
	100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
};
DescriptLedBlinking indHitting = {(int*)blinkHitting, (int*)tempHitting, sizeof(blinkHitting)/sizeof(int)};


const int blinkDisconect[] = {
	1,0,1,0,1,0,
};
const int tempDisconect[] = {
	100, 100, 100, 100, 100, 1500,
};
DescriptLedBlinking indDisconect = {(int*)blinkDisconect, (int*)tempDisconect, sizeof(blinkDisconect)/sizeof(int)};


const int blinkGameStoped[] = {
	1,0
};
const int tempGameStoped[] = {
	700, 1400,
};
DescriptLedBlinking indGameStoped = {(int*)blinkGameStoped, (int*)tempGameStoped, sizeof(blinkGameStoped)/sizeof(int)};

const int blinkKilledLong[] = {
	1,0
};
const int tempKilledLong[] = {
	5000, 800,
};
DescriptLedBlinking indKilledLong = {(int*)blinkKilledLong, (int*)tempKilledLong, sizeof(blinkKilledLong)/sizeof(int)};

const int blinkKilledShort[] = {
	1,0
};
const int tempKilledShort[] = {
	50, 400,
};
DescriptLedBlinking indKilledShort = {(int*)blinkKilledShort, (int*)tempKilledShort, sizeof(blinkKilledShort)/sizeof(int)};

const int blinkEvent[] = {
	1,0,1,0
};
const int tempEvent[] = {
	150, 150, 150, 150
};
DescriptLedBlinking indEvent = {(int*)blinkEvent, (int*)tempEvent, sizeof(blinkEvent)/sizeof(int)};

const int blinkDevOff[] = {
	0, 1, 0
};
const int tempDevOff[] = {
	100, 250, 200,
};
DescriptLedBlinking indDevOff = {(int*)blinkDevOff, (int*)tempDevOff, sizeof(blinkDevOff)/sizeof(int)};

const int blinkKilledLighter[] = {
	1,0
};
const int tempKilledLighter[] = {
	50, 800,
};
DescriptLedBlinking indKilledLighter = {(int*)blinkKilledLighter, (int*)tempKilledLighter, sizeof(blinkKilledLighter)/sizeof(int)};


/*indication of calibration rfm module. Start*/
const int blinkCalibration[] = {
	1,0
};
const int tempCalibration[] = {
	100, 100,
};
DescriptLedBlinking indCalibration = {(int*)blinkCalibration, (int*)tempCalibration, sizeof(blinkCalibration)/sizeof(int)};
/*indication of calibration rfm module. The end*/


/*Indication snapping. Start*/
const int blinkSnapping[] = {
	0,    1,   0,  1,   0,  1,  0,  1, 0,
}; 
const int tempSnapping[] = {
	200, 50, 100, 50, 800, 50, 100, 50, 1500,
};
DescriptLedBlinking indSnapping = {(int*)blinkSnapping, (int*)tempSnapping, sizeof(blinkSnapping)/sizeof(int)};
/*Indication snapping. The end*/


/*PZRK indication begin*/
const int blinkAimingProcessing[] = {
	1, 0
};
const int tempAimingProcessing[] = {
	200, 200
};
DescriptLedBlinking indAimingProcessing = {(int*)blinkAimingProcessing, (int*)tempAimingProcessing, sizeof(blinkAimingProcessing)/sizeof(int)};

const int blinkAimingAlly[] = {
	1, 0
};
const int tempAimingAlly[] = {
	40, 40
};
DescriptLedBlinking indAimingAlly = {(int*)blinkAimingAlly, (int*)tempAimingAlly, sizeof(blinkAimingAlly)/sizeof(int)};

const int blinkAimingDone[] = {
	1
};
const int tempAimingDone[] = {
	1000
};
DescriptLedBlinking indAimingDone = {(int*)blinkAimingDone, (int*)tempAimingDone, sizeof(blinkAimingDone)/sizeof(int)};
/*PZRK indication end*/









