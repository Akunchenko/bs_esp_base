#ifndef __LED_LIBRARY__
#define __LED_LIBRARY__

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stdlib.h"

typedef enum{
	LED_OFF = 0x00U,
	LED_ON = 0x01U,
}StatusLed;

typedef enum{
	LED_RESET =	0x00U,
	LED_READY =	0x01U,
	LED_BLINKING = 0x02U,
	LED_PAUZE =	0x03U
}StateLedBlinking;

typedef enum{
	LED_MODE_DISCONTINUOUS = 0x00U, //после достижения конца файла индикации (Led.file) индикация останавливается;
	LED_MODE_CONTINUOUS = 0x01U, // //после достижения конца файла индикации (Led.file) все начинается сначала;
}WorkModeLed;

typedef struct{
	 int *state;
	 int *duration;
	 size_t sizeBuf;
}DescriptLedBlinking;

struct DescriptLed{
	void (*TurnOnLed)(void);
	void (*TurnOffLed)(void);
	void (*IndicationIsOver)(DescriptLedBlinking *_file);
	uint32_t (*GetTimeInms)(void);
};

typedef struct{
	struct DescriptLed descr;
	StatusLed status;
	StateLedBlinking stateBlinking;
	uint8_t numberStateInFile;
	uint32_t timeChangeState;
	DescriptLedBlinking file;
	WorkModeLed mode;
}Led;


void LED_Init(Led *_led, struct DescriptLed *_descr);
bool LED_SetTask(Led *_led, DescriptLedBlinking *file, WorkModeLed _mode);
bool LED_SetOn(Led *_led, uint32_t timeout);
void LED_Run(Led *_led);
void LED_ClearTask(Led *_led);

extern DescriptLedBlinking indMiddleLevelBat;
extern DescriptLedBlinking indCriticalLowLevelBat;
extern DescriptLedBlinking indModeVibro;
extern DescriptLedBlinking indSnakesGreen;
extern DescriptLedBlinking indSnakesYellow;
extern DescriptLedBlinking indSnakesOrange;
extern DescriptLedBlinking indSnakesRed;
extern DescriptLedBlinking indErrorActivationTM62;
extern DescriptLedBlinking indErrorDeactivationTM62;
extern DescriptLedBlinking indErrorExplosionTM62;
extern DescriptLedBlinking indConstStateOn;
	
extern DescriptLedBlinking indHitting;
extern DescriptLedBlinking indDisconect;
extern DescriptLedBlinking indGameStoped;
extern DescriptLedBlinking indKilledLong;
extern DescriptLedBlinking indKilledShort;
extern DescriptLedBlinking indEvent;
extern DescriptLedBlinking indDevOff;
extern DescriptLedBlinking indKilledLighter;
extern DescriptLedBlinking indCalibration;
extern DescriptLedBlinking indSnapping;
extern DescriptLedBlinking indAimingDone;
extern DescriptLedBlinking indAimingProcessing;
extern DescriptLedBlinking indAimingAlly;

#ifdef __cplusplus
}
#endif
#endif

