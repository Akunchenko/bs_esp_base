#ifndef _PERIPHERY_h
#define _PERIPHERY_h
#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class Periphery
{
public:
	enum mcpPinouts {
		ENABLE_DC = 0,
		BUZZER = 1,
		POW_BUTTON = 2,
		CHECK_STDBY = 3,
		CHECK_CHARGE = 4,
		ENABLE_MEASURING = 5,
	};

	void SetBuzzerState(bool state);
	void SetDcDcState(bool state);
	void SetMeasuringState(bool state);
	
	uint8_t GetPowerButtonState();
	uint8_t GetChargeDoneState();
	void ControlDisablingBoard(void);
	uint8_t GetChargeState();

	void ControlEnablingBoard(void);

	Periphery();
	~Periphery();

private:
	
};
extern Periphery outward;

#endif

