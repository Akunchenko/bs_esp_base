#include "Periphery.h"

#include "Adafruit_MCP23017.h"

#include "Indication.h"

#include <power_control\src\power_control.h>


/*
* Connect pin #12 of the expander to Analog 5 (i2c clock)
* Connect pin #13 of the expander to Analog 4 (i2c data)
* Connect pins #15, 16 and 17 of the expander to ground (address selection)
* Connect pin #9 of the expander to 5V (power)
* Connect pin #10 of the expander to ground (common ground)
* Connect pin #18 through a ~10kohm resistor to 5V (reset pin, active low)
* Output #0 is on pin 21 so connect an LED or whatever from that to ground
*/
Adafruit_MCP23017 mcp;
/*
* Declare our NeoPixel strip object:
* Argument 1 = Number of pixels in NeoPixel strip
* Argument 2 = Arduino pin number (most are valid)
* Argument 3 = Pixel type flags, add together as needed:
*   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
*   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
*   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
*   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
*   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
*/

#define POWER_OFF_TIME 1500
PowerControlContex pwConfig;


uint32_t GetMilis() {
	return millis();
}
void InitPowerControl(void);
Periphery::Periphery()
{
	
	mcp.begin();      // use default address 0

	mcp.pinMode(ENABLE_DC, OUTPUT);
	mcp.pinMode(BUZZER, OUTPUT);
	mcp.pinMode(ENABLE_MEASURING, OUTPUT);
	mcp.pinMode(CHECK_CHARGE, INPUT);
	mcp.pinMode(CHECK_STDBY, INPUT);
	mcp.pinMode(POW_BUTTON, INPUT);

	mcp.pullUp(CHECK_CHARGE, HIGH);
	mcp.pullUp(CHECK_STDBY, HIGH);

	InitPowerControl();
	
	Serial.println("Periphery Init");
	
	//SetDcDcState(HIGH);
	SetMeasuringState(HIGH);
}
Periphery outward = Periphery();
Periphery::~Periphery()
{
}
void Periphery::SetBuzzerState(bool state) {
	mcp.digitalWrite(BUZZER, state);
}
void Periphery::SetDcDcState(bool state) {
	mcp.digitalWrite(ENABLE_DC, state);
}
void Periphery::SetMeasuringState(bool state) {
	mcp.digitalWrite(ENABLE_MEASURING, state);
}

uint8_t Periphery::GetPowerButtonState() {
	return mcp.digitalRead(POW_BUTTON);
}
uint8_t Periphery::GetChargeState() {
	return mcp.digitalRead(CHECK_CHARGE);
}
uint8_t Periphery::GetChargeDoneState() {
	return mcp.digitalRead(CHECK_STDBY);
}

// Rainbow cycle along whole strip. Pass delay time (in ms) between frames.


void Periphery::ControlDisablingBoard(void) {
	PWC_CheckDisable(GetMilis);
	
}
void Periphery::ControlEnablingBoard(void) {
	PWC_CheckEnable(GetMilis);
}
PWC_StatePin GetStatePowerButtonPin(void)
{
	PWC_StatePin res;
	if (outward.GetPowerButtonState()) {
		res = PWC_STATE_PRESSED;
	}
	else {
		res = PWC_STATE_RELEASED;
	}
	return res;
}

void DisableBoardClbk(void)
{
	static uint32_t timeoutDeactivating;
	uint32_t timeOff = millis() + POWER_OFF_TIME;
	indication.StopLeds();
	indication.powerOnOFF();
	
	while (timeOff > millis()) {
		indication.Handler();
	}
	Serial.println("Disable pwr");
}

void EnableBoardClbk(void)
{
	outward.SetDcDcState(HIGH);
	Serial.println("Enable pwr");
	indication.powerOnOFF();
}

void WaitingBoardClbk(void)
{
}

bool SetPowerBoardClbk(SetPowerControl set)
{
	if (set == PWC_SET_DISABLE) {
		outward.SetDcDcState(LOW);
	}
	return true;
}

void InitPowerControl(void)
{
	pwConfig.config.timeout_disable = 1000;
	pwConfig.config.timeout_enable = 2000;
	pwConfig.config.timeout_error = 5000;
	pwConfig.config.resolution_disable = 1;
	pwConfig.type_button = PWC_TYPE_BUT_WITHOUT_FIX;

	pwConfig.disable_clbk = DisableBoardClbk;
	pwConfig.enable_clbk = EnableBoardClbk;
	pwConfig.wait_enable_clbk = WaitingBoardClbk;
	pwConfig.get_state_button = GetStatePowerButtonPin;
	pwConfig.set_power = SetPowerBoardClbk;

	PWC_InitPowerControl(&pwConfig);
	//PWC_SetPowerEnable();
}
