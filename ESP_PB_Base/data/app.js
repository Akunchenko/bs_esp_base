'use strict';

(function () {
    function init() {
        var router = new Router([
            new Route('scoreboard', 'scoreboard.htm', true),
            new Route('settings', 'settings.htm'),            
            new Route('about', 'about.htm'),
            
        ]);
    }
    init();
}());


  
function pushMe(){
  var settingsForm = document.querySelector('form');
  var settingsInputNumber = settingsForm.querySelectorAll('input');
  for (var i = 0; i < settingsInputNumber.length; i++) {
    var current =  document.getElementsByClassName("settings__input_number");
    
    current[i].setAttribute('autocomplete', 'off');
    console.log(current[i]);
  }
  settingsForm.addEventListener("submit", function (event) {
    // Каждый раз, когда пользователь пытается отправить данные, мы проверяем
     // валидность поля электронной почты.
      onsole.log('submit');
      event.preventDefault();
  }, false);
  
}


var x = document.getElementById("myAudio"); 
function playAudio() { 
  x.play(); 
} 

function pauseAudio() { 
  x.pause(); 
} 
function startTime(){
  var today=new Date();
  var h=today.getHours();
  var m=today.getMinutes();
  var s=today.getSeconds();
 
  // add a zero in front of numbers<10
  m=checkTime(m);
  s=checkTime(s);
  //document.getElementById('countGT').innerHTML=  m + ":" + s;
  setTimeout('startTime()',1000);
}

function checkTime(i){
  if (i<10){
    i = "0" + i;
  }
  return i;
}


// Get all buttons with class="btn" inside the container
var btns = document.getElementsByClassName("menu__button");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");

    // If there's no active class
    if (current.length > 0) {
      current[0].className = current[0].className.replace(" active", "");
    }

    // Add the active class to the current/clicked button
    this.className += " active";
  });
}


function give(val) {
  var inner = document.getElementById(val);
  if (inner.value.length >= 2) inner.value = inner.value.substr(0, 2); // в поле можно ввести только 2 символов
 
}

function restart(submit, texts) {
  if (confirm(texts)) {
      server = "/restart?device=ok";
      send_request(submit, server);
      return true;
  } else {
      return false;
  }
}

