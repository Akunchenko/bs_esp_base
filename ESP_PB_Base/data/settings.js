//selecting dom elements for manipulation
var input = document.querySelector("input[type = 'text']");
var ul = document.querySelector("ul");
var container = document.querySelector("div");
var lists = document.querySelectorAll("li");
var spans = document.getElementsByTagName("span");
var saveBtn = document.querySelector(".save");
var clearBtn = document.querySelector(".clear");
var closeBtn = document.querySelector(".closeBtn");
var closes = document.getElementsByClassName("close");
var select = document.querySelector(".selectSett");
var inputLimit = document.querySelector(".settings__input_number");
var formCountId = 1;

//function to delete todo if delete span is clicked.
function deleteTodo() {
    for (let item of closes) {
        item.addEventListener("click", function () {
            item.parentElement.remove();
            event.stopPropagation();
        });
    }
}

//function to load todo if list is found in local storage.
function loadTodo() {
    if (localStorage.getItem('todoList')) {
        ul.innerHTML = localStorage.getItem('todoList');
        deleteTodo();
    }
}

//event listener for input to add new todo to the list.
input.addEventListener("keypress", function (keyPressed) {
    if (keyPressed.which === 13) {
        createNewElement();
    }
});
inputLimit.addEventListener("keypress", function (keypress) {
    console.log(inputLimit);
    if(inputLimit.value >= 2){
        inputLimit.value = inputLimit.value.substr(0, 2);
    }
})
function createBtnElem(_inputValue) {
    var btn = document.createElement("BUTTON");
    btn.innerHTML = "Settings for " + _inputValue;
    btn.className = "collapsible";
    btn.addEventListener("click", function () {
        this.classList.toggle("active");
        var tmp_content1 = this.nextElementSibling;
        var tmp_content2 = tmp_content1.nextElementSibling;
        var tmp_content3 = tmp_content2.nextElementSibling;
        var content = tmp_content3.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        }
    });

   return btn;
}

function createDivContent() {
    var divContent = document.createElement("DIV");
    //divContent.innerHTML = "Lorem ipsum dolor sit amet, conasd afdaws fgtasrdf awrefasdf asdfsadf efwerf esfefw ff 245 tjhfjkslif up;AOND FIPOHFG8GADSKFNM IPOUG VU";
    divContent.innerHTML = markup;
    divContent.setAttribute("id", "form" + formCountId++);
    divContent.className = "content";
   
    return divContent;
}

function createSpanClose() {
    var span = document.createElement("SPAN");
    //var txt = document.createTextNode("\u00D7");
    span.innerHTML = "X";
    span.className = "close";
    span.appendChild(txt);
    return span;
}
function createBtn(_txt, _className) {
    var btn = document.createElement("BUTTON");
    btn.innerHTML = _txt;
    btn.className = _className;
    return btn;
}
function createInput(_type, _className) {
    var data = document.createElement("INPUT");
    data.type = _type;
    data.className = _className;
    data.addEventListener('change', e => {
        e.target.parentElement.classList.toggle('checked');
        if(e.target.checked){
            //do something
        }
    });
    return data;
}

function createNewElement() {
    var li = document.createElement("li");
    var inputValue = document.getElementById("myInput").value;
    if (inputValue === '') {
        alert("You must write something!");
    } else {
        ul.appendChild(li);
    }
    document.getElementById("myInput").value = "";
    li.appendChild(createBtnElem(inputValue));
    li.appendChild(createBtn("Save","saveBtn"));
    li.appendChild(createBtn("X", "close"));
    li.appendChild(createInput("checkbox", "selectSett"))
    formCountId++;
    /*li.appendChild(createDivContent());
    var testCont = document.querySelector(".content");
    testCont.innerHTML = markup;*/
    //li.appendChild(CreateFormSettings());
    li.appendChild(CreateCloneOfForm());
    deleteTodo();
}

// event listener to linethrough list if clicked
ul.addEventListener('click', function (ev) {
    if (ev.target.tagName === 'LI') {
        //ev.target.classList.toggle('checked');
    }
}, false
);
select.addEventListener('change', e => {
    
    console.log(e.target.parentElement.classList);
    e.target.parentElement.classList.toggle('checked');
    
    if(e.target.checked){
        //do something
    }

});
//save todolist state so user can access it later
saveBtn.addEventListener('click', function () {
    localStorage.setItem('todoList', ul.innerHTML);

});

//clear all todo when clear button is clicked
clearBtn.addEventListener('click', function () {
    ul.innerHTML = "";
    localStorage.removeItem('todoList', ul.innerHTML);
});

//delete todo
deleteTodo();

//load Todo
loadTodo();

var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var tmp_content = this.nextElementSibling;
        var tmp_content2 = tmp_content.nextElementSibling;
        var tmp_content3 = tmp_content2.nextElementSibling;
        var content = tmp_content3.nextElementSibling;
        if (content.style.maxHeight) {
          content.style.maxHeight = null;
        } else {
          content.style.maxHeight = content.scrollHeight + "px";
        }
      });
    }

   
    var markup = `
    <div class="settings__form">
    <!--<button onclick="pushMe()">PUSH ME</button>-->
    <form id="myform" action="/save.html" method="GET">
        <div class="settings__container" >
            <div class="settings__list" id="grid-settings">
                <div class="settings__item settings__title">( 1 )Game Time</div>
                <div class="settings__item"><label for="gameMin" class="settings__label">( 2 ) min<input type="number"  value="05" min="1" max="99" name="gameMin" id="gameMin" oninput="give(this.id)" class="settings__input_number"></label></div>
                <div class="settings__item"><label for="gameSec" class="settings__label">( 3 ) sec<input type="number" value="00" min="0" max="60" name="gameSec" id="gameSec" oninput="give(this.id)" class="settings__input_number"></label></div>
                
                <div class="settings__item settings__title">( 4 ) Condition to win</div>
                <div class="settings__item"><label for="CTW_rise" class="settings__label">( 5 ) CTW_rise<input type="number" value="01" min="1" max="60" name="CTW_rise" id="CTW_rise" oninput="give(this.id)" class="settings__input_number"></label></div>
                <div class="settings__item"><label for="CTW_mercy" class="settings__label">( 6 ) CTW_mercy<input type="number" value="01" min="1" max="60" name="CTW_mercy" id="CTW_mercy" oninput="give(this.id)" class="settings__input_number"></label></div>
                <div class="settings__item"><label for="switchSide" class="settings__label">( 7 ) Switch side each:</label>
                <select id="switchSide" name="switchSide">
                    <option value="1"> 1 </option>
                    <option value="2"> 2 </option>
                    <option value="3"> 3 </option>
                    <option value="4"> 4</option>
                </select>
                </div>
                <div class="settings__item settings__title">( 8 ) Timeout Time</div>
                <div class="settings__item"><label for="timeOutMin" class="settings__label">( 9 ) min<input type="number" value="01" min="1" max="99" name="timeOutMin" id="timeOutMin" oninput="give(this.id)" class="settings__input_number"></label></div>
                <div class="settings__item"><label for="timeOutSec" class="settings__label">( 10 ) sec<input type="number" value="30" min="0" max="60" name="timeOutSec" id="timeOutSec" oninput="give(this.id)" class="settings__input_number"></label></div>
                <div class="settings__item"><label for="chkOverTime" class="settings__label">( 11 ) <input type="checkbox" name="chkOverTime" id="chkOverTime" class="settings__input">chkOverTime</label></div>
                
                <div class="settings__item settings__title">( 12 )Overtime Time</div>
                <div class="settings__item"><label for="overTimeMin" class="settings__label">( 13 ) min<input type="number" value="05" min="1" max="99" name="overTimeMin" id="overTimeMin" oninput="give(this.id)" class="settings__input_number"></label></div>
                <div class="settings__item"><label for="overTimeSec" class="settings__label">( 14 ) sec<input type="number" value="00" min="0" max="60" name="overTimeSec" id="overTimeSec" oninput="give(this.id)" class="settings__input_number"></label></div>
                
                <div class="settings__item settings__title">( 15 ) Break Time</div>
                <div class="settings__item"><label for="breakTimeMin" class="settings__label">( 16 ) min<input type="number" value="05" min="1" max="99" name="breakTimeMin" id="breakTimeMin" oninput="give(this.id)" class="settings__input_number"></label></div>
                <div class="settings__item"><label for="breakTimeSec" class="settings__label">( 17 ) sec<input type="number" value="00" min="0" max="60" name="breakTimeSec" id="breakTimeSec" oninput="give(this.id)" class="settings__input_number"></label></div>
                
                <div class="settings__item settings__title">( 18 )Type of Game</div>
                <div class="settings__item"><label for="typeOfGameXball" class="settings__label">( 19 ) <input type="radio" name="typeOfGameXball" id="typeOfGameXball" class="settings__input">Xball</label></div>
                <div class="settings__item"><label for="typeOfGameTactic" class="settings__label">( 20 ) <input type="radio" name="typeOfGameTactic" id="typeOfGameTactic" class="settings__input">Tactic</label></div>
                <div class="settings__item"><label for="typeOfGameTrainMode" class="settings__label">( 21 ) <input type="radio" name="typeOfGameTrainMode" id="typeOfGameTrainMode" class="settings__input">TrainMode</label></div>
                
                <div class="settings__item settings__title">( 22 )Team game</div>
                <div class="settings__item"><label for="teamGameSingle" class="settings__label">( 23 ) <input type="radio" name="teamGameSingle" id="teamGameSingle" class="settings__input">Single</label></div>
                <div class="settings__item"><label for="teamGameDual" class="settings__label">( 24 ) <input type="radio" name="teamGameDual" id="teamGameDual" class="settings__input">Dual</label></div>
                <div class="settings__item"><label for="team_1_Name" class="settings__label">( 25 ) team_1_Name<input type="text" name="team_1_Name" id="team_1_Name" class="settings__input_text" pattern="[A-Za-z]"></label></div>
                <div class="settings__item"><label for="team_2_Name" class="settings__label">( 26 ) team_2_Name<input type="text" name="team_2_Name" id="team_2_Name" class="settings__input_text" pattern="[A-Za-z]"></label></div>
                <div class="settings__item"><button type="submit" value="Save settings" class="settings__save">( 27 ) Save settings</button></div>
            </div>
        </div>
    </form>
</div>
    `;
    
function createDivElement(_txt, _className, _id) {
    var _data = document.createElement("DIV");
    _data.innerHTML = _txt;
    _data.className = _className;
    _data.setAttribute("id", _id);
    return _data;
}

function createLableElement(_for, _txt, _className, _id) {
    var _data = document.createElement("LABEL");
    _data.innerHTML = _txt;
    _data.className = _className;
    _data.setAttribute("for", _for);
    _data.setAttribute("id", _id);
    return _data;
}
function createInputElement(_type, _className, _name, _value, _minNumb, _maxNumb, _id) {
    var _data = document.createElement("INPUT");
    _data.className = _className;
    _data.setAttribute("type", _type);
    _data.setAttribute("name", _name);
    _data.setAttribute("value", _value);
    _data.setAttribute("min", _minNumb);
    _data.setAttribute("max", _maxNumb);
    _data.setAttribute("id", _id);
    return _data;
 
}
function createFormElement(_method, _action, _id, _name = "", _target) {
    var _data = document.createElement("FORM");
    _data.setAttribute("name", _name);
    _data.setAttribute("method", _method);
    _data.setAttribute("action", _action);
    _data.setAttribute("target", _target);
    _data.setAttribute("id", _id);

    return _data;
}

 function CreateCloneOfForm() {
    
    var _content = createDivElement("","content","");
    var element = document.querySelector( '.example__form' ).cloneNode( true );
    
    element.classList.add( 'settings__form' );
    element.classList.remove( 'example__form' );
    
    var x = element.childNodes;
    x[1].setAttribute("id", "myform" + formCountId);

    console.log(x);
    _content.appendChild(element);
    return _content;
 }

 var xhr = new XMLHttpRequest();

 var json = JSON.stringify({
   "name": "Victor",
   "surname": "ZOY",
   "sensor":"gps",
   "time":1351824120,
   "data":[48.75608,2.302038]
 });
 
 xhr.open("POST", '/config/set', true)
 xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
 
 xhr.onreadystatechange =  function () {
    if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
        console.log(xhr.responseText);
    };
};
 
 // Отсылаем объект в формате JSON и с Content-Type application/json
 // Сервер должен уметь такой Content-Type принимать и раскодировать
 xhr.send(json);