
#ifndef _LOGIC_h
#define _LOGIC_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif
	class MyLogic
	{
	public:
		enum TypeOfLoraDevice
		{
			BUTTONS = 1,
			TOWELS = 101,
		};
		enum SysStates_e
		{
			INIT_PEREPHERY = 0,
			INIT_POWER,
			INIT_WIFI,
			INIT_SYSTEM
		};
		bool isCharging;
		bool isStandBy;
		uint8_t sysState;
		MyLogic();
		void Handler();
		void PinpointBuzzerTime(uint32_t time);

		
	private:
		uint32_t _timerOffBuzzer;
	};





#endif