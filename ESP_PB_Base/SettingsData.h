#pragma once
#include <Arduino.h>
class SettingsData
{
public:
    SettingsData();
    SettingsData(int _gameTimeMin);
    SettingsData(int _gameTimeMin, int _gameTimeSec, int _timeOutMin, int _timeOutSec,
        int _overTimeMin, int _overTimeSec, int _breakTimeMin, int _breakTimeSec,
        byte _conditionToWin, byte _switchSideEach,
        String _typeOfGame, String _teamGame, uint8_t _count, String _data, int _numb);
    int getGameTimeMin();
    int getGameTimeSe();
    byte getConditionToWin();
    byte getSwitchSideEach();
    int getTimeOutMin();
    int getTimeOutSec();
    int getOverTimeMin();
    int getOverTimeSec();
    int getBreakTimeMin();
    int getBreakTimeSec();
    String getTypeOfGame();
    String getTeamGame();
    String getTeamNames(byte);

    void setGameTimeMin(int);
    void setGameTimeSec(int);
    void setConditionToWin(byte);
    void setSwitchSideEach(byte);
    void setTimeOutMin(int);
    void setTimeOutSec(int);
    void setOverTimeMin(int);
    void setOverTimeSec(int);
    void setBreakTimeMin(int);
    void setBreakTimeSec(int);

    void setTypeOfGame(String);
    void setTeamGame(String);
    void createBufTeamNames(int _count);
    void setTeamNames(String, int);
private:

    int gameTimeMin, gameTimeSec, timeOutMin, timeOutSec, overTimeMin, overTimeSec, breakTimeMin, breakTimeSec;
    byte conditionToWin, switchSideEach;
    String typeOfGame;
    String teamGame;
    String *teamNames;
};

