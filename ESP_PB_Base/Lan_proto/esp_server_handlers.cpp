#include "esp_server_handlers.h"
#include "esp.pb.h"
//#include "runtime_dbg.h"

#include "flash_utils.h"
#include <Esp.h>

extern int32_t spiffs_hal_write(uint32_t addr, uint32_t size, uint8_t *src);
extern int32_t spiffs_hal_erase(uint32_t addr, uint32_t size);
extern int32_t spiffs_hal_read(uint32_t addr, uint32_t size, uint8_t *dst);

namespace esp_srv
{
  void OnEspRebootReceived()
  {
    ESP.restart();
  }
  

  void SettingsReceived(const Settings* msg)
  {
    /*EspVersionReply reply = { 0 };
    VersionInfo fw_info = FM_GetBuildInfo();
    reply.firmware_ver.ptr = reinterpret_cast<char *>(fw_info.data);
    reply.firmware_ver.len = FW_DATA_SIZE;
    SendEspVersionReply(&reply);*/
  }
  
  void PingReceived() {
      SendPing();
  }
  
  void init()
  {
    Serial.printf("Start ESP Server INIT\n");
    esp_handlers.OnSettingsReceived = SettingsReceived;
    esp_handlers.OnPingReceived = PingReceived;
    //esp_srv_handlers.OnEspSomeDataReceived = nullptr;//OnSomeDataReceived;
    //esp_srv_handlers.OnEspRuntimeErrorReceived   = PROTO_FS_CLBK_NAME(EspRuntimeErrorReceived);
    //esp_srv_handlers.OnEspVersionRequestReceived = PROTO_FS_CLBK_NAME(EspVersionRequestReceived);

    //esp_srv_handlers.OnFsInfoReceived       = PROTO_FS_CLBK_NAME(FsInfoReceived);
    //esp_srv_handlers.OnFormatFsReceived     = PROTO_FS_CLBK_NAME(FormatFsReceived);
    //esp_srv_handlers.OnLsDirReceived        = PROTO_FS_CLBK_NAME(LsDirReceived);
    //esp_srv_handlers.OnFileInfoReceived     = PROTO_FS_CLBK_NAME(FileInfoReceived);
    //esp_srv_handlers.OnDelFileReceived      = PROTO_FS_CLBK_NAME(DelFileReceived);
    //esp_srv_handlers.OnReadFileReceived     = PROTO_FS_CLBK_NAME(ReadFileReceived);
    //esp_srv_handlers.OnWriteFileReceived    = PROTO_FS_CLBK_NAME(WriteFileReceived);
    //esp_srv_handlers.OnEspFirmwareTaskReceived = PROTO_FS_CLBK_NAME(EspFirmwareTaskReceived);
    //esp_srv_handlers.OnCalcMd5Received      = PROTO_FS_CLBK_NAME(CalcMd5Received);


  }

} // namespace esp_srv