#pragma once

#include <IPAddress.h>

#include "protolib.h"

extern "C" int BinCommandSplitter(MemStream* stream, uint32_t* pCmdId);

namespace udp_io
{
  // Data types

  enum UdpIOFlags_T : uint8_t
  {
    F_SYN = (1 << 0),
    F_ACK = (1 << 1),
    F_DAT = (1 << 2),
    F_RST = (1 << 3),
  };

  enum UdpIOStates_T 
  {
    UIO_ST_CLOSED = 0,
    UIO_ST_STAGE1 = 1,
    UIO_ST_ESTABL = 2,
  };

  typedef void(*on_connected_clbk_ptr_t)();

  typedef void(*on_closed_clbk_ptr_t)();

  typedef bool(*on_cmd_received_clbk_ptr_t)(MemStream *);

  void EspConnectToAp(const char *_ssid, const char *_pass);
  void init(on_connected_clbk_ptr_t _conn_clbk_ptr, on_closed_clbk_ptr_t _clos_clbk_ptr, on_cmd_received_clbk_ptr_t _cmd_rec_clbk_ptr);

  void update();

  unsigned availableForWrite();

  unsigned writeBytes(const char *buffer, const size_t length);
  bool WriteToUdpSendBuffer(MemStream* _stream, const size_t data_size);
  bool isConnected();

  UdpIOStates_T status();

  void stop();

  void connect(const IPAddress &_ip_addr, const uint16_t _port);

  void OnServerDiscoveredClbk(const IPAddress& _ip_addr, const uint16_t _port, const uint32_t _server_id, const uint8_t _priority, const string _name);
} // namespace udp_io