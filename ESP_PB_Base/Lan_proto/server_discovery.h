#pragma once

#include <IPAddress.h>
#include <inttypes.h>
#include <protolib.h>

namespace serv_dis
{
  const unsigned SERVER_ANN_PORT_START = 4000;
  const unsigned SERVER_ANN_PORT_COUNT = 3;
  
  typedef void(*server_discovery_clbk_ptr_t)(const IPAddress &_ip_addr, const uint16_t _port, const uint32_t _server_id, const uint8_t _priority, const string _name);

  void init(server_discovery_clbk_ptr_t _clbk_ptr);
  void askServersToAnnounce(const uint8_t _dev_type, const string _serial_num);
  void askServersToAnnounce(const uint8_t _dev_type, const string _serial_num, const IPAddress& _ip_addr);
} // namespace serv_dis