//-----------------------------------------------------------------//
//  Developed by Vladimir Sosedkin for Forpost(c) company in 2018  //
//  email: sosed.mail@gmail.com                                    //
//-----------------------------------------------------------------//

#include "task_sheduler.h"
#include <list>
#include <algorithm>
#include <iterator>

#ifdef ARDUINO
#include <Arduino.h>
const auto get_time = millis;
#else // not ARDUINO

#endif // ARDUINO

#ifndef TASK_SHEDULER_MAX_TASKS_COUNT
#define TASK_SHEDULER_MAX_TASKS_COUNT 4
#endif

namespace
{
	struct ShedulerTask_T
	{
		void *context;
		sheduler_run_clbk_t run_clbk;
	};

	std::list<ShedulerTask_T> tasks;
	unsigned tasks_count = 0;
}


extern "C" bool TaskSheduler_Exec(void *_context, sheduler_run_clbk_t _clbk)
{
	if (TASK_SHEDULER_MAX_TASKS_COUNT == tasks_count)
		return false;
	
	ShedulerTask_T t = {_context, _clbk};
	tasks.push_back(t);
	++tasks_count;
	return true;
}


extern "C" bool TaskSheduler_ExecTimeout(
  void               *_task_context, 
  sheduler_run_clbk_t _clbk, 
  time_expired_clbk_t _handle_expired, 
  const uint32_t      _timeout)
{
  struct Context
  {
    void               *task_context;
    sheduler_run_clbk_t task_clbk;
    time_expired_clbk_t expired_clbk;
    uint32_t            timeout;
    uint32_t            timestamp;
  };

  auto context          = new Context;
  context->task_clbk    = _clbk;
  context->task_context = _task_context;
  context->expired_clbk = _handle_expired;
  context->timeout      = _timeout;
  context->timestamp    = get_time();

  auto task = [](void *_ct)
  {
    Context *ct = static_cast<Context *>(_ct);

    if (!ct->task_clbk(ct->task_context))
    {
      if (get_time() - ct->timestamp < ct->timeout)
        return false;
			
			if (nullptr != ct->expired_clbk)
				ct->expired_clbk(ct->task_context);
    }

    delete ct;
    return true;
  };

  return TaskSheduler_Exec(context, task);
}


void TaskSheduler_CompleteOrPostponeImpl(
    void               *_task_context,
    uint32_t            _cnt_size,
    sheduler_run_clbk_t _task_clbk,
    fail_clbk_t         _fail_clbk,
    const uint32_t      _timeout)
{
  if (_task_clbk(_task_context))
    return;

  struct Context
  {
    void               *task_context;
    sheduler_run_clbk_t task_clbk;
    fail_clbk_t         fail_clbk;
  };

  Context *cnt = new Context;
  if (nullptr == cnt)
  {
    _fail_clbk(_task_context);
    return;
  }

  cnt->task_context = ::operator new(_cnt_size);
  if (nullptr == cnt->task_context)
  {
    delete cnt;
    _fail_clbk(_task_context);
    return;
  }
  memcpy(cnt->task_context, _task_context, _cnt_size);
  cnt->task_clbk = _task_clbk;
  cnt->fail_clbk = _fail_clbk;

  auto task = [](void * context)
  {
    Context *ct = static_cast<Context *>(context);
    if (!ct->task_clbk(ct->task_context))
      return false;

    ::operator delete(ct->task_context);
    delete ct;
    return true;
  };

  auto expir = [](void * context)
  {
    Context *ct = static_cast<Context *>(context);
    ct->fail_clbk(ct->task_context);
    ::operator delete(ct->task_context);
    delete ct;
  };

  if (TaskSheduler_ExecTimeout(cnt, task, expir, _timeout))
    return;
  
  ::operator delete(cnt->task_context);
  delete cnt;
  _fail_clbk(_task_context);
}


extern "C" bool TaskSheduler_Remove(const void *_context, const sheduler_run_clbk_t _clbk)
{
  unsigned last_tasks_count = tasks_count;
  auto remove_predicate = [_context, _clbk](ShedulerTask_T &t)
  {
    if (_context != t.context || _clbk != t.run_clbk)
      return false;

    --tasks_count;
    return true;
  };

  tasks.remove_if(remove_predicate);

  return last_tasks_count > tasks_count;
}


extern "C" unsigned TaskSheduler_RemoveAll(sheduler_run_clbk_t _run_clbk, context_handler_clbk_t _handle_context)
{
  unsigned count = 0;

  auto remove_predicate = [&count, _run_clbk, _handle_context](ShedulerTask_T &t)
  {
    if (t.run_clbk != _run_clbk)
      return false;
    _handle_context(t.context);
    ++count;
    --tasks_count;
    return true;
  };
  tasks.remove_if(remove_predicate);

  return count;
}


extern "C" void TaskSheduler_Update(void)
{
  auto remove_predicate = [](ShedulerTask_T &t)
  {
    if (!t.run_clbk(t.context))
      return false;
    --tasks_count;
    return true;
  };

	tasks.remove_if(remove_predicate);
}
