//-----------------------------------------------------------------//
//  Developed by Vladimir Sosedkin for Forpost(c) company in 2019  //
//  email: sosed.mail@gmail.com                                    //
//-----------------------------------------------------------------//

#pragma once
#ifndef FP_PROTO_FS_H
#define FP_PROTO_FS_H

#include <stdbool.h>
#include <stdint.h>
#ifdef ESP8266
#include "esp_srv.pb.h"
//#include "HardwareSerial.h"
#else // not ESP8266
#include "filesystem.pb.h"
#include "firmware.pb.h"
#include "flash_writer.h"
#endif //ESP8266

#ifdef ESP8266

#define PROTO_SEND_CLBK(_BASE_NAME) Send ## _BASE_NAME
#define PROTO_FS_MOD_PREFIX(_BASE_NAME) _BASE_NAME
#define PROTO_FS_CLBK_NAME(_BASE_NAME) On ## _BASE_NAME

#else

#define PROTO_SEND_CLBK(_BASE_NAME) SendStm ## _BASE_NAME
#define PROTO_FS_MOD_PREFIX(_BASE_NAME) Stm ## _BASE_NAME
#define PROTO_FS_CLBK_NAME(_BASE_NAME) OnStm ## _BASE_NAME

#endif

typedef bool(*writeFileTaskClbk_t)(void *_context, const PROTO_FS_MOD_PREFIX(WriteFile) *msg);

typedef struct
{
  FpFilesystemContext_T *fs_cnt;
	bool (*flash_erase_clbk)(const uint32_t _addr, const uint32_t _len);
	bool (*flash_read_clbk) (const uint32_t _addr, uint8_t *_buf, const uint32_t _len);
	bool (*flash_write_clbk)(const uint32_t _addr, const uint8_t *_buf, const uint32_t _len);
  
  void               *writeFileContext;
  writeFileTaskClbk_t writeFileTask;
} ProtoFSConfig_T;


#ifdef __cplusplus
extern "C" {
#endif

void InitProtoFs(ProtoFSConfig_T *_cfg, FpFilesystemContext_T *_fs_cnt);

void ResetProtoFs(void);

void PROTO_FS_CLBK_NAME(FsInfoReceived)      (void);
void PROTO_FS_CLBK_NAME(FormatFsReceived)    (void);
void PROTO_FS_CLBK_NAME(LsDirReceived)       (const PROTO_FS_MOD_PREFIX(LsDir)        *msg);
void PROTO_FS_CLBK_NAME(FileInfoReceived)    (const PROTO_FS_MOD_PREFIX(FileInfo)     *msg);
void PROTO_FS_CLBK_NAME(DelFileReceived)     (const PROTO_FS_MOD_PREFIX(DelFile)      *msg);
void PROTO_FS_CLBK_NAME(ReadFileReceived)    (const PROTO_FS_MOD_PREFIX(ReadFile)     *msg);
void PROTO_FS_CLBK_NAME(WriteFileReceived)   (const PROTO_FS_MOD_PREFIX(WriteFile)    *msg);
void PROTO_FS_CLBK_NAME(CalcMd5Received)     (const PROTO_FS_MOD_PREFIX(CalcMd5)      *msg);
void PROTO_FS_CLBK_NAME(EspFirmwareTaskReceived)(const PROTO_FS_MOD_PREFIX(EspFirmwareTask) *msg);

#ifndef ESP8266
#define BOOT_FIRMWARE_ADDR 0x8009000
#define	BOOT_FIRMWARE_MAX_SIZE (0x9000 - sizeof(uint32_t))
#define BOOT_FIRMWARE_SIZE_ADDR (BOOT_FIRMWARE_ADDR + BOOT_FIRMWARE_MAX_SIZE)

bool FlashWriter_EraseClbk(uint32_t _page_addr, uint32_t _len);

bool FlashWriter_WriteClbk(uint32_t _address, const uint8_t *_data, uint32_t _size);

void WriteBootloaderSize(const FlashWriterCfg_T *flash_writer, const uint32_t _size);

void TryUpdateBootloader(void);

#endif

#ifdef __cplusplus
}
#endif

#endif // FP_PROTO_FS_H
