#ifndef _CALLBACKS_h
#define _CALLBACKS_h

#include "protolib.h"

void OnConnectionClosed();
void OnConnectionEstablished();
bool OnFullCmdReceived(MemStream* _stream);
void SendLikePing(void);

#endif //_CALLBACKS_h