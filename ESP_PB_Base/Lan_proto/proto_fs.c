//-----------------------------------------------------------------//
//  Developed by Vladimir Sosedkin for Forpost(c) company in 2019  //
//  email: sosed.mail@gmail.com                                    //
//-----------------------------------------------------------------//

#include "proto_fs.h"
#include <stdlib.h>
#include "task_sheduler.h"
#include "md5.h"
#include "firmware_header.h"
#ifdef ESP8266
#include "flash_config.h"
#include "eboot_command.h"
#include <spiffs/spiffs_nucleus.h>
#include "flash_writer.h"
#else // not ESP8266
#include <spiffs_nucleus.h>
#include "common.pb.h"
#include "stm32f1xx_hal.h"
#include <internal_flash.h>
#include "description.slaves.h"
#include "transfer.h"	
#include "dispatcher.msg.h"
#include "main_sub_ghz.h"
extern int32_t GetUuid32(void);
#endif // ESP8266

#define SEND_TIMEOUT_MSEC 3000

#define EXT_FLASH_PAGE_SIZE 0x1000 // 4kb page

static const char* FS_NAME = "spiffs";

typedef enum { FIRMWARE_DEFAULT, FIRMWARE_UPDATE, REGULAR_FILE } FirmwareType_t; 

#ifdef ESP8266

#define PROTO_SEND(_BASE_NAME, _MSG) SendEsp ## _BASE_NAME(_MSG)
#define GET_SYS_TIME_MSEC() millis()

#define UPDATE_FIRMWARE_FILENAME  "/fw/updesp.bin"
#define DEFAULT_FIRMWARE_FILENAME "/fw/defesp.bin"

#else // not ESP8266

#define PROTO_SEND(_BASE_NAME, _MSG) SendStm ## _BASE_NAME(_MSG)
#define GET_SYS_TIME_MSEC() HAL_GetTick()

#define UPDATE_FIRMWARE_FILENAME      "/fw/updstm.bin"
#define DEFAULT_FIRMWARE_FILENAME     "/fw/defstm.bin"
#define BOOT_FIRMWARE_FILENAME        "/fw/stmboot.bin"
#define INSTALL_BOOT_FILENAME         "/fw/inst_stm_boot"

#define UPDATE_FIRMWARE_HB50_FILENAME	"/fw/upd_main_hb.bin"
#define BOOT_FIRMWARE_HB50_FILENAME	  "/fw/boot_hb.bin"

#define UPDATE_FIRMWARE_HB44_FILENAME	"/fw/upd_main_hb44.bin" 
#define BOOT_FIRMWARE_HB44_FILENAME	  "/fw/boot_hb44.bin"

#endif // ESP8266

#define CREATE_CONTEXT(_CNT_TYPE) \
	_CNT_TYPE *context = (_CNT_TYPE *)malloc(sizeof(_CNT_TYPE)); \
	if (NULL == context) \
		return; \
  else \
	  memset(&context->reply, 0, sizeof(context->reply));

#define WRITE_FILE_RESPONCE_TIMEOUT_MSEC 300

ProtoFSConfig_T *g_cfg;

void InitProtoFs(ProtoFSConfig_T *_cfg, FpFilesystemContext_T *_fs_cnt)
{
  _cfg->fs_cnt = _fs_cnt;
	g_cfg = _cfg;
}

//-----------------------------------------------------------------------------------------------------------

void OnFsInfoResponceFailed(void *_context)
{
	//#TODO
}

void PROTO_FS_CLBK_NAME(FsInfoReceived)(void) // override __weak
{
	PROTO_FS_MOD_PREFIX(FsInfoReply) reply = {0};
	SetString(&reply.fsname, FS_NAME);
	
	spiffs * fs = FpFilesystem_GetFs(g_cfg->fs_cnt);
	
	reply.blockSize     = SPIFFS_CFG_PHYS_ERASE_SZ(fs);
	reply.maxOpenFiles  = SPIFFS_MAX_FILES;
	reply.maxPathLength = SPIFFS_OBJ_NAME_LEN;
	reply.pageSize      = SPIFFS_CFG_LOG_PAGE_SZ(fs);
	
	bool is_success = SPIFFS_OK == SPIFFS_info(fs, &reply.totalBytes, &reply.usedBytes);
	reply.status = is_success ? FS_Status_Ok : FS_Status_Failed;
	
	TaskSheduler_CompleteOrPostpone(reply, PROTO_SEND_CLBK(EspFsInfoReply), OnFsInfoResponceFailed, SEND_TIMEOUT_MSEC);
}

//-----------------------------------------------------------------------------------------------------------

enum { FMT_FS_STAGE1,	FMT_FS_STAGE2, FMT_FS_STAGE3, FMT_FS_STAGE_SEND, FMT_FS_STAGE_DEFAULT };
enum { SEND_PROGRESS_DELTA = 10 };
typedef struct
{
	uint8_t         stage;
	int             last_progress;
	uint32_t        timestamp;
	spiffs_block_ix bix;
	PROTO_FS_MOD_PREFIX(FormatFsReply) reply;
} FormatContext_T;

void CloseFormatContext(void *_context)
{
  free(_context);
}

s32_t SPIFFS_incremental_format(spiffs *fs, spiffs_block_ix *_bix) 
{
  SPIFFS_API_CHECK_CFG(fs);
  if (SPIFFS_CHECK_MOUNT(fs)) {
    fs->err_code = SPIFFS_ERR_MOUNTED;
    return -1;
  }

  s32_t res;

	fs->max_erase_count = 0;
	res = spiffs_erase_block(fs, (*_bix));
	if (res != SPIFFS_OK) {
		res = SPIFFS_ERR_ERASE_FAIL;
	}
	SPIFFS_API_CHECK_RES_UNLOCK(fs, res);
	
	return 100 * (++(*_bix)) / fs->block_count;
}

bool FormatFsTask(void *_context)
{
	FormatContext_T *ct = (FormatContext_T *)_context;
	
	switch(ct->stage)
	{
		case FMT_FS_STAGE1:
			FpFilesystem_Unmount(g_cfg->fs_cnt);
			ct->reply.status = FS_Status_ProcessStarted;
			ct->stage = FMT_FS_STAGE2;
			return false;
		
		case FMT_FS_STAGE2:
			{
				SPIFFS_LOCK(FpFilesystem_GetFs(g_cfg->fs_cnt));
				int progress;
				if (0 > (progress = SPIFFS_incremental_format(FpFilesystem_GetFs(g_cfg->fs_cnt), &ct->bix)))  // error occured
				{
					ct->reply.status = FS_Status_Failed;
					ct->stage        = FMT_FS_STAGE_SEND;
					ct->timestamp    = GET_SYS_TIME_MSEC();
					return false;
				}
				SPIFFS_UNLOCK(FpFilesystem_GetFs(g_cfg->fs_cnt));
				
				if (SEND_PROGRESS_DELTA < progress - ct->reply.status ||
					  FS_Status_ProcessCompleted <= progress)
				{
					ct->reply.status = progress;
          PROTO_SEND(FormatFsReply, &ct->reply);
				}
				
				if (FS_Status_ProcessCompleted <= progress) // format completed
					ct->stage = FMT_FS_STAGE3;
			}
			return false;
			
		case FMT_FS_STAGE3:
			ct->reply.status = FpFilesystem_Mount(g_cfg->fs_cnt) ? FS_Status_MountOk : FS_Status_MountFailed;
			ct->stage        = FMT_FS_STAGE_SEND;
			ct->timestamp    = GET_SYS_TIME_MSEC();
			return false;
			
		case FMT_FS_STAGE_SEND:
			if (!PROTO_SEND(FormatFsReply, &ct->reply) && 
				  GET_SYS_TIME_MSEC() - ct->timestamp < SEND_TIMEOUT_MSEC)
				return false;
			// fall throught		
	}
	
	free(ct);
	return true;
}

void PROTO_FS_CLBK_NAME(FormatFsReceived)(void) // override __weak
{
	CREATE_CONTEXT(FormatContext_T)
	context->stage = FMT_FS_STAGE1;
	context->bix   = 0;
	
	if (!TaskSheduler_Exec(context, FormatFsTask))
	{
    CloseFormatContext(context);
		// #TODO send error responce
	}
};

//-----------------------------------------------------------------------------------------------------------

enum { LS_DIR_MAX_FILES = 3 };
typedef enum { LS_DIR_STAGE_INIT, LS_DIR_STAGE_READ,	LS_DIR_STAGE_SEND } LsDirStage_T;
typedef struct
{
	LsDirStage_T stage;
	spiffs_DIR   dir;
	uint32_t     timestamp;
	char         fnames[LS_DIR_MAX_FILES][SPIFFS_OBJ_NAME_LEN];
	PROTO_FS_MOD_PREFIX(DirReply)   files[LS_DIR_MAX_FILES];
	PROTO_FS_MOD_PREFIX(LsDirReply) reply;
} LsDirContext_T;

void CloseLsDirContext(void *_context)
{
	LsDirContext_T *ct = (LsDirContext_T *)_context;
	if (NULL != ct->dir.fs)
		SPIFFS_closedir(&ct->dir);
	free(_context);
	SPIFFS_clearerr(FpFilesystem_GetFs(g_cfg->fs_cnt));
}

bool LsDirTask(void *_context)
{
	LsDirContext_T *ct = (LsDirContext_T *)_context;
	
	if (LS_DIR_STAGE_READ == ct->stage)
	{
		struct spiffs_dirent de;
		ct->reply.files.count = 0;
		while(NULL != SPIFFS_readdir(&ct->dir, &de))
		{
      PROTO_FS_MOD_PREFIX(DirReply) *f = &ct->files[ct->reply.files.count];
			f->page_ix = de.pix;
			f->size    = de.size;
			strcpy(ct->fnames[ct->reply.files.count], (char *)de.name);
			SetString(&f->filename, ct->fnames[ct->reply.files.count]);
			
			if (LS_DIR_MAX_FILES == ++ct->reply.files.count)
				break;
		}
		
		if (SPIFFS_OK == FpFilesystem_GetError(g_cfg->fs_cnt))
			ct->reply.status = FS_Status_InProcess;
		else if (SPIFFS_VIS_END == FpFilesystem_GetError(g_cfg->fs_cnt))
			ct->reply.status = FS_Status_ProcessCompleted;
		else
			ct->reply.status = FS_Status_Failed;
		
		ct->timestamp = GET_SYS_TIME_MSEC();
	}
	
	ct->stage = PROTO_SEND(LsDirReply, &ct->reply) ? LS_DIR_STAGE_READ : LS_DIR_STAGE_SEND;
  if (LS_DIR_STAGE_SEND == ct->stage)
	{
		if (GET_SYS_TIME_MSEC() - ct->timestamp > SEND_TIMEOUT_MSEC)
		{
			CloseLsDirContext(_context);
			return true;
		}
		
		return false;
	}
	
	if (FS_Status_InProcess == ct->reply.status)
		return false;
	
	CloseLsDirContext(_context);
	return true;
}


void PROTO_FS_CLBK_NAME(LsDirReceived)(const PROTO_FS_MOD_PREFIX(LsDir)* msg) // override __weak
{
  CREATE_CONTEXT(LsDirContext_T)

  context->dir.fs = NULL;
	
	SPIFFS_clearerr(FpFilesystem_GetFs(g_cfg->fs_cnt));
  if (NULL == SPIFFS_opendir(FpFilesystem_GetFs(g_cfg->fs_cnt), msg->path.ptr, &context->dir))
	{
		context->reply.status = FS_Status_Failed;
		context->stage        = LS_DIR_STAGE_SEND;
		context->timestamp    = GET_SYS_TIME_MSEC();
	}
	else
	{
		context->stage = LS_DIR_STAGE_READ;
		context->reply.files.ptr = context->files;
	}
	
	if (!TaskSheduler_Exec(context, LsDirTask))
	{
		CloseLsDirContext(context);
		// #TODO send error responce
	}
};

//-----------------------------------------------------------------------------------------------------------

void OnFileInfoResponceFailed(void *_context)
{
	//#TODO
}

void PROTO_FS_CLBK_NAME(FileInfoReceived)(const PROTO_FS_MOD_PREFIX(FileInfo) *msg) // override __weak
{
	PROTO_FS_MOD_PREFIX(FileInfoReply) reply;
	spiffs_stat stat;
  if (SPIFFS_OK == SPIFFS_stat(FpFilesystem_GetFs(g_cfg->fs_cnt), msg->filename.ptr, &stat))
	{
		reply.page_ix = stat.pix;
		reply.size    = stat.size;
		reply.status  = FS_Status_Ok;
	}
	else
		reply.status = FS_Status_Failed;
	
	TaskSheduler_CompleteOrPostpone(reply, PROTO_SEND_CLBK(EspFileInfoReply), OnFileInfoResponceFailed, SEND_TIMEOUT_MSEC);
};

//-----------------------------------------------------------------------------------------------------------

void OnDelFileResponceFailed(void *_context)
{
	//#TODO
}

void PROTO_FS_CLBK_NAME(DelFileReceived)(const PROTO_FS_MOD_PREFIX(DelFile) *msg) // override __weak
{
	PROTO_FS_MOD_PREFIX(DelFileReply) reply;
	spiffs_file fh;
	bool is_success = 0 < (fh  = SPIFFS_open_by_page(FpFilesystem_GetFs(g_cfg->fs_cnt), msg->page_ix, SPIFFS_O_RDWR, 0)) && 
		                SPIFFS_OK == SPIFFS_fremove(FpFilesystem_GetFs(g_cfg->fs_cnt), fh);
	reply.status = is_success ? FS_Status_Ok : FS_Status_Failed;
	TaskSheduler_CompleteOrPostpone(reply, PROTO_SEND_CLBK(EspDelFileReply), OnDelFileResponceFailed, SEND_TIMEOUT_MSEC);
};

//-----------------------------------------------------------------------------------------------------------

#define READ_FILE_BYTES 128
#define READ_FILE_DELAY_MSEC 50
typedef enum { READ_FILE_STAGE_READ,	READ_FILE_STAGE_SEND } ReadFileStage_T;
typedef struct
{
	ReadFileStage_T stage;
	int             timestamp;
	uint32_t        length;
	spiffs_file     f_handl;
	char            rbuf[READ_FILE_BYTES];
	PROTO_FS_MOD_PREFIX(ReadFileReply) reply;
} ReadFileContext_T;

void CloseReadFileContext(void *_context)
{
	ReadFileContext_T *ct = (ReadFileContext_T *)_context;
	if (0 <= ct->f_handl)
		SPIFFS_close(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->f_handl);
	free(ct);
}

bool ReadFileTask(void *_context)
{
	ReadFileContext_T *ct = (ReadFileContext_T *)_context;
	
	if (READ_FILE_STAGE_READ == ct->stage)
	{
		int bytes_to_read  = sizeof(ct->rbuf) > ct->length ? ct->length : sizeof(ct->rbuf);
		int readed_bytes   = SPIFFS_read(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->f_handl, ct->rbuf, bytes_to_read);
		
		ct->reply.data.ptr = ct->rbuf;
		ct->reply.data.len = (0 > readed_bytes) ? 0 : readed_bytes;
		ct->length -= ct->reply.data.len;
		
		if (0 > readed_bytes)
			ct->reply.status = FS_Status_Failed;
		else if (0 == ct->length)
			ct->reply.status = FS_Status_ProcessCompleted;
		else
			ct->reply.status = FS_Status_InProcess;	
	}
	
	if (READ_FILE_STAGE_SEND == ct->stage &&
		  GET_SYS_TIME_MSEC() - ct->timestamp < READ_FILE_DELAY_MSEC)
		return false;
	
	ct->stage = PROTO_SEND(ReadFileReply, &ct->reply) ? READ_FILE_STAGE_READ : READ_FILE_STAGE_SEND;
  if (READ_FILE_STAGE_SEND == ct->stage)
	{
		ct->timestamp = GET_SYS_TIME_MSEC();
		return false;
	}
	
	if (FS_Status_InProcess == ct->reply.status)
		return false;
	
	CloseReadFileContext(ct);
	return true;
}

void PROTO_FS_CLBK_NAME(ReadFileReceived)(const PROTO_FS_MOD_PREFIX(ReadFile) *_msg) // override __weak
{
	CREATE_CONTEXT(ReadFileContext_T)
	
	if (0 < (context->f_handl = SPIFFS_open_by_page(FpFilesystem_GetFs(g_cfg->fs_cnt), _msg->page_ix, SPIFFS_O_RDONLY, 0)))
		SPIFFS_lseek(FpFilesystem_GetFs(g_cfg->fs_cnt), context->f_handl, _msg->offset, SPIFFS_SEEK_SET);
	
	if (SPIFFS_OK != FpFilesystem_GetError(g_cfg->fs_cnt))
	{
		context->reply.status = FS_Status_Failed;
		PROTO_SEND(ReadFileReply, &context->reply);
		free(context);
		return;
	}
	
	context->stage  = READ_FILE_STAGE_READ;
	context->length = _msg->lenght;
	if (!TaskSheduler_Exec(context, ReadFileTask))
	{
		free(context);
		// #TODO send error responce
	}
};

//-----------------------------------------------------------------------------------------------------------

typedef enum { FwWrite_Stage_Write, FwWrite_Stage_Complete } FwWriteStages_t;

typedef struct
{
  FwWriteStages_t                     stage;
  PROTO_FS_MOD_PREFIX(WriteFileReply) reply;
	FlashWriterCfg_T                    flash_writer;
	FirmwareHeaderContext_T             fw_header;
	FirmwareType_t                      fw_type;
	spiffs_file                         fh;
	uint32_t                            timestamp;
} WriteFileContext_T;

bool WriteFileTask(void *_context, const PROTO_FS_MOD_PREFIX(WriteFile) *msg);
bool WriteResponceTask(void *_context);

void OpenWriteFileContext(const PROTO_FS_MOD_PREFIX(WriteFile) *_msg,  void **_context, writeFileTaskClbk_t *_callback)
{
	CREATE_CONTEXT(WriteFileContext_T);
	context->fw_type = REGULAR_FILE;
	context->stage   = FwWrite_Stage_Write;
	
	spiffs_flags f = (SPIFFS_O_CREAT | SPIFFS_O_WRONLY);
	if (_msg->flags & WM_Rewrite)
		f |= SPIFFS_O_TRUNC;
	else if (_msg->flags & WM_Append)
		f |= SPIFFS_O_APPEND;
	
	spiffs_file fh;
	if (0 >= (fh = SPIFFS_open(FpFilesystem_GetFs(g_cfg->fs_cnt), _msg->filename.ptr, f, 0)))
	{
		free(context);
		PROTO_FS_MOD_PREFIX(WriteFileReply) reply = {0};
		reply.status = FS_Status_Failed;
		if (!PROTO_SEND(WriteFileReply, &reply))
		{
			// #TODO Set Error flag
		}
		return;
	}

	context->fh  = fh;
	(*_context)  = context;
	(*_callback) = WriteFileTask;
}

bool FindExtFlashAddrForWrite(FirmwareType_t _fw_type, FirmwareHeaderContext_T *_header)
{
  if (FirmwareHeader_Open(_header, FP_EXT_FLASH_FIRMWARE_1_HEADER_ADDR))
	{
		bool is_default = FirmwareHeader_IsDefault(_header);
		
		if (FIRMWARE_DEFAULT == _fw_type && is_default)
		  return true;
		
		if (FIRMWARE_UPDATE == _fw_type && !is_default)
		  return true;
	}
	
	return FirmwareHeader_Open(_header, FP_EXT_FLASH_FIRMWARE_2_HEADER_ADDR);
}

inline uint32_t GetExtFlashFirmwareSizeMax(uint32_t _flash_addr)
{
	return FP_EXT_FLASH_FIRMWARE_1_ADDR == _flash_addr ? 
		FP_EXT_FLASH_FIRMWARE_1_MAX_SIZE : 
		FP_EXT_FLASH_FIRMWARE_2_MAX_SIZE;
}

bool WriteFirmwareTask(void *_context, const PROTO_FS_MOD_PREFIX(WriteFile) *_msg)
{
	WriteFileContext_T *ct = (WriteFileContext_T *)_context;
	
	switch (ct->stage)
  {
		case FwWrite_Stage_Write:
			ct->reply.status = FlashWriter_Write(&ct->flash_writer, (uint8_t *)_msg->data.ptr, _msg->data.len) ? 
				FS_Status_InProcess : 
				FS_Status_Failed;
			
			ct->reply.written = FlashWriter_GetWritten(&ct->flash_writer);
		
			if (FS_Status_Failed == ct->reply.status)
				break; // Send fail status and exit
			
			if (_msg->flags & WM_Close)
			{
				ct->stage        = FwWrite_Stage_Complete;
				ct->reply.status = FS_Status_Ok; 
				
				if (_msg->flags & WM_Cancel)
					FirmwareHeader_SetFlags(&ct->fw_header, F_DELETED_FIRMWARE);
				else
				{
					if (FirmwareHeader_SetSize(&ct->fw_header, ct->flash_writer.written))
					{
						FlashWriter_Finish(&ct->flash_writer);
						if (!FlashWriter_Write(&ct->flash_writer, FlashWriter_GetMd5(&ct->flash_writer), MD5_HASH_SIZE) ||
								!FirmwareHeader_SetFlags(&ct->fw_header, F_UPLOADING_COMPLETED))
							ct->reply.status = FS_Status_Failed;
					}
					else
						ct->reply.status = FS_Status_Failed;
				}
			}
			break;
			
		case FwWrite_Stage_Complete:
			break; // Send reply status and exit

		default: // unexpected status, unknow error occured!
			ct->reply.status = FS_Status_Failed;
  }

  TaskSheduler_Remove(_context, WriteResponceTask);
	ct->timestamp = GET_SYS_TIME_MSEC();
	if (!PROTO_SEND_CLBK(EspWriteFileReply)(&ct->reply) && !TaskSheduler_Exec(_context, WriteResponceTask))
	{
		//#TODO should set RuntimeError here
		return true;
	}
	
  return false;
}

void OpenWriteContext(
	const PROTO_FS_MOD_PREFIX(WriteFile) *_msg, 
	void **_context, writeFileTaskClbk_t *_callback)
{
	FirmwareType_t fw_type = REGULAR_FILE;
	if (0 == strcmp(_msg->filename.ptr, UPDATE_FIRMWARE_FILENAME))
		fw_type = FIRMWARE_UPDATE;
	else if (0 == strcmp(_msg->filename.ptr, DEFAULT_FIRMWARE_FILENAME))
		fw_type = FIRMWARE_DEFAULT;
	
	if (REGULAR_FILE == fw_type)
	{
		OpenWriteFileContext(_msg, _context, _callback);
		return;
	}
	
	CREATE_CONTEXT(WriteFileContext_T)
	context->fw_type = fw_type;
	context->stage   = FwWrite_Stage_Write;
	
	bool is_success = false;
	//{
		FirmwareHeader_Init(&context->fw_header, g_cfg->flash_read_clbk, g_cfg->flash_write_clbk);
		is_success             = FindExtFlashAddrForWrite(fw_type, &context->fw_header);
		uint32_t firmware_addr = FirmwareHeader_GetFirmwareAddr(&context->fw_header);
		
		is_success = is_success && 
								 FlashWriter_Begin(
									 &context->flash_writer, 
									 EXT_FLASH_PAGE_SIZE, 
									 firmware_addr, 
									 GetExtFlashFirmwareSizeMax(firmware_addr),
									 g_cfg->flash_erase_clbk, 
									 g_cfg->flash_write_clbk);
		uint32_t flags = F_UPLOADING_STARTED | (FIRMWARE_DEFAULT == fw_type ? F_DEFAULT_FIRMWARE : 0);
		is_success = is_success && FirmwareHeader_ResetFlags(&context->fw_header, flags);
	//}
	
  if (!is_success)
	{
		context->stage        = FwWrite_Stage_Complete;
		context->reply.status = FS_Status_Failed;
	}
	
	(*_context)  = context;
	(*_callback) = WriteFirmwareTask;
}

void CloseWriteContext(void **_context, writeFileTaskClbk_t *_callback)
{
  if (NULL == (*_context))
    return;

  if (WriteFileTask == (*_callback))
		SPIFFS_close(FpFilesystem_GetFs(g_cfg->fs_cnt), ((WriteFileContext_T *)(*_context))->fh);
	
	free(*_context);
	(*_context)  = NULL;
	(*_callback) = NULL;
}

bool WriteResponceTask(void *_context)
{
	WriteFileContext_T *ct = (WriteFileContext_T *)_context;
	
	if (GET_SYS_TIME_MSEC() - ct->timestamp < WRITE_FILE_RESPONCE_TIMEOUT_MSEC ||
		  !PROTO_SEND_CLBK(EspWriteFileReply)(&ct->reply))
		return false;
	
	if (FS_Status_Failed == ct->reply.status || FwWrite_Stage_Complete == ct->stage)
		CloseWriteContext(&g_cfg->writeFileContext, &g_cfg->writeFileTask);
	
	return true;
}

bool WriteFileTask(void *_context, const PROTO_FS_MOD_PREFIX(WriteFile) *_msg)
{
	WriteFileContext_T *ct = (WriteFileContext_T *)_context;
	
	switch (ct->stage)
  {
		case FwWrite_Stage_Write:
			ct->reply.status = _msg->offset == SPIFFS_lseek(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->fh, _msg->offset, SPIFFS_SEEK_SET) ? 
				FS_Status_InProcess : 
				FS_Status_Failed;
		
			if (FS_Status_Failed != ct->reply.status)
				ct->reply.status = _msg->data.len == SPIFFS_write(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->fh, _msg->data.ptr, _msg->data.len) ? 
					FS_Status_InProcess : 
					FS_Status_Failed;
		
			if (FS_Status_Failed != ct->reply.status)
			{
				int32_t written = SPIFFS_tell(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->fh);
				if (0 > written) 
					ct->reply.status = FS_Status_Failed;
				else
					ct->reply.written = written;
			}
			
			if (FS_Status_Failed == ct->reply.status)
				break; // Send fail status and exit
			
			if (_msg->flags & WM_Close)
			{
			  ct->reply.status = FS_Status_Ok; 
			  ct->stage        = FwWrite_Stage_Complete;
			}				
			break;
			
		case FwWrite_Stage_Complete:
			break; // Send reply status and exit

		default: // unexpected status, unknow error occured!
			ct->reply.status = FS_Status_Failed;
  }

	TaskSheduler_Remove(_context, WriteResponceTask);
	ct->timestamp = GET_SYS_TIME_MSEC();
	if (!PROTO_SEND_CLBK(EspWriteFileReply)(&ct->reply) && !TaskSheduler_Exec(_context, WriteResponceTask))
	{
		//#TODO should set RuntimeError here
		return true;
	}
	
  return false;
}

void PROTO_FS_CLBK_NAME(WriteFileReceived)(const PROTO_FS_MOD_PREFIX(WriteFile) *msg) // override __weak
{
	if (0 < msg->filename.len)
	{	
		CloseWriteContext(&g_cfg->writeFileContext, &g_cfg->writeFileTask);
		OpenWriteContext(msg, &g_cfg->writeFileContext, &g_cfg->writeFileTask);
	}
	
	if (NULL == g_cfg->writeFileContext)
	{
		PROTO_FS_MOD_PREFIX(WriteFileReply) reply = { 0 };
		reply.status = FS_Status_Failed;
		PROTO_SEND_CLBK(EspWriteFileReply)(&reply);
		return;
	}
	
	if (NULL != g_cfg->writeFileContext && g_cfg->writeFileTask(g_cfg->writeFileContext, msg))
		CloseWriteContext(&g_cfg->writeFileContext, &g_cfg->writeFileTask);
};

//-----------------------------------------------------------------------------------------------------------

typedef enum { CALC_MD5_STAGE1, CALC_MD5_STAGE2, CALC_MD5_STAGE3, CALC_MD5_STAGE_SEND } CalcMd5Stage_T;

typedef struct 
{
	CalcMd5Stage_T stage;
	spiffs_file    fh;
	md5_context_t  md5_cnt;
	uint32_t       timestamp;
	uint8_t        buf[64];
	PROTO_FS_MOD_PREFIX(CalcMd5Reply) reply;
} CalcMd5Context_T;

void CloseCalcMd5Context(void *_context)
{
	CalcMd5Context_T *ct = (CalcMd5Context_T*)_context;
	if (0 <= ct->fh)
		SPIFFS_close(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->fh);
	free(_context);
}

bool CalcMd5Task(void *_context)
{
	CalcMd5Context_T *ct = (CalcMd5Context_T*)_context;
	
	switch(ct->stage)
	{
		case CALC_MD5_STAGE1:
		if (0 > ct->fh)
		{
			ct->timestamp = GET_SYS_TIME_MSEC();
			ct->stage     = CALC_MD5_STAGE_SEND;
		}
		else
		{
			MD5Init(&ct->md5_cnt);
			ct->stage = CALC_MD5_STAGE2;
		}
		return false;
			
		case CALC_MD5_STAGE2:
		{
			int readed;
			if (0 < (readed = SPIFFS_read(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->fh, ct->buf, sizeof(ct->buf))))
				MD5Update(&ct->md5_cnt, ct->buf, readed);
			else
				ct->stage = CALC_MD5_STAGE3;
			return false;
		}
		
		case CALC_MD5_STAGE3:
		{
			MD5Final(ct->buf, &ct->md5_cnt);
			ct->reply.checksum.ptr = (char *)ct->buf;
			ct->reply.checksum.len = MD5_HASH_SIZE;
			ct->stage = CALC_MD5_STAGE_SEND;
			return false;
		}
		
		case CALC_MD5_STAGE_SEND:
			if (!PROTO_SEND(CalcMd5Reply, &ct->reply) && 
				  GET_SYS_TIME_MSEC() - ct->timestamp < SEND_TIMEOUT_MSEC)
				return false;
			// fall throught		
	}
	
	CloseCalcMd5Context(_context);
	return true;
}

void PROTO_FS_CLBK_NAME(CalcMd5Received)(const PROTO_FS_MOD_PREFIX(CalcMd5) *_msg) // override __weak
{
	CREATE_CONTEXT(CalcMd5Context_T)
	context->fh    = SPIFFS_open_by_page(FpFilesystem_GetFs(g_cfg->fs_cnt), _msg->page_ix, SPIFFS_O_RDONLY, 0);
	context->stage = CALC_MD5_STAGE1;
	
	if (!TaskSheduler_Exec(context, CalcMd5Task))
	{
		CloseCalcMd5Context(context);
		//#TODO Set Error flag
	}
}

//-----------------------------------------------------------------------------------------------------------

void OnInstallFirmwareFailed(void *_context)
{
	//#TODO
}

#ifdef ESP8266

void SetEspBootCommand(uint32_t _firmware_addr, uint32_t _firmware_size)
{
  struct eboot_command ebcmd = { 0 };
  ebcmd.action = ACTION_COPY_RAW;
  ebcmd.args[0] = _firmware_addr;
  ebcmd.args[1] = 0x00000;
  ebcmd.args[2] = _firmware_size;
  eboot_command_write(&ebcmd);
}

#else
typedef struct{
	DescriptionSlave *slave;
	int8_t task_id;
	spiffs_file fh;
	int8_t last_send_status;
}UpdateSlaveContext;

static bool UpdateSlave_IsTaskSetBySlave(DescriptionSlave *_slave){ // HACK
	return TR_IsRunning(_slave);
}

static char* UpdateSlave_GetNameFile(SlavesTypeDevice_E _type, int8_t _task){
	char *name = NULL;

	if(_type 	== 	TypeDevice_Hb_5_0		||
		 _type	==	TypeDevice_Vest_12	||
		 _type	==	TypeDevice_Vest_10){

		if(FirmwareTaskUpdate == _task)
			name 	= UPDATE_FIRMWARE_HB50_FILENAME;
		if(FirmwareTaskInstallBootloader	==	_task)
			name	=	BOOT_FIRMWARE_HB50_FILENAME;
	}

	if(_type 	== 	TypeDevice_Hb_4_4){

		if(FirmwareTaskUpdate == _task)
			name 	= UPDATE_FIRMWARE_HB44_FILENAME;
		if(FirmwareTaskInstallBootloader	==	_task)
			name	=	BOOT_FIRMWARE_HB44_FILENAME;
//			name 	= UPDATE_FIRMWARE_HB44_FILENAME;// for test
	}
	return name;
}

static SubGhz_NameFile_E GetNameFileBySlaveProtocol(int8_t _task){
	SubGhz_NameFile_E name = SubGhz_FileNameNotDefined;
	if(FirmwareTaskUpdate == _task)
		name	=	 SubGhz_MainFirmware;
	if(FirmwareTaskInstallBootloader	==	_task)
		name = SubGhz_BootFirmware;

	return name;
}

static SubGhz_FirmwareTask_E GetTaskBySlaveProtocol(int8_t _task){
	SubGhz_FirmwareTask_E task = SubGhz_FirmwareTaskEmpty;

	if(FirmwareTaskUpdate == _task)
		task = SubGhz_FirmwareTaskUpdate;
	if(FirmwareTaskInstallBootloader	==	_task)
		task = SubGhz_FirmwareTaskInstallBootloader;
	
	return task;
}
static int ReadClbkTransfer(DescriptionSlave *_slave, UpdateSlaveContext *ct, const int32_t _offset,char *_buf, const size_t _size){
	int res = SPIFFS_lseek(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->fh, _offset,SPIFFS_SEEK_SET);
	if(res >= 0)
		res = SPIFFS_read(FpFilesystem_GetFs(g_cfg->fs_cnt), ct->fh, _buf, _size);
	return res;
}

static int WrClbkTransfer(DescriptionSlave *_slave,UpdateSlaveContext *ct, const int32_t _offset,char *_buf, const size_t _size){
	
	if(SendToSlaveRfWriteFile(_slave->AddrRfSlave,GetNameFileBySlaveProtocol(ct->task_id),_buf,_size,_offset)){
		TR_SetPause(_slave);
		return _size;
	}
	return 0;
}

static bool UpdateSlave_Task(UpdateSlaveContext *_ct){
	PROTO_FS_MOD_PREFIX(FirmwareTaskReply) reply;
	reply.dev_id = _ct->slave->dev_id;
 	reply.task_id = _ct->task_id;

	TR_Update(_ct->slave,_ct);

	if(!TR_IsFailTask(_ct->slave)){

	reply.status = TR_GetProgress(_ct->slave);
	if(((int8_t)reply.status - _ct->last_send_status) > 5 &&
			 PROTO_SEND_CLBK(EspFirmwareTaskReply)(&reply))
		_ct->last_send_status = reply.status;

		if(TR_IsDoneTask(_ct->slave)){
			reply.status = SendToSlaveRfTaskUpdate(_ct->slave->AddrRfSlave,GetTaskBySlaveProtocol(_ct->task_id)) ? 
				FS_Status_ProcessCompleted : FS_Status_Failed;

			goto CloseTask;
		}
		return false;
	}

	reply.status = FS_Status_Failed;
	goto CloseTask;


	CloseTask:
		SPIFFS_close(FpFilesystem_GetFs(g_cfg->fs_cnt), _ct->fh);
		PROTO_SEND_CLBK(EspFirmwareTaskReply)(&reply);
		TR_DeInitDynamic(_ct->slave);
		free(_ct);
		SetTimingByAddrClient(AddrRegistr,300,5000,300,40);
		return true;
}

static bool UpdateSlave_SetTaskUpdate(DescriptionSlave *_slave, int8_t _task_id){
	spiffs_stat stat;

	if (SPIFFS_OK != SPIFFS_stat(FpFilesystem_GetFs(g_cfg->fs_cnt), UpdateSlave_GetNameFile(_slave->Type,_task_id), &stat)	||
			UpdateSlave_IsTaskSetBySlave(_slave))
		return false;

	UpdateSlaveContext *cnt = malloc(sizeof(UpdateSlaveContext));
	if(cnt == NULL)
		return false;

	cnt->fh = SPIFFS_open_by_page(FpFilesystem_GetFs(g_cfg->fs_cnt), stat.pix, SPIFFS_RDWR, 0);
	if(0	>	cnt->fh)
		return false;

	TransferDescr descr = {.rd_clbk = (read_clbk_tr)ReadClbkTransfer, .wr_clbk = (write_clbk_tr)WrClbkTransfer};
	if(!TR_InitDynamic(&descr,stat.size,_slave))
		return false;

	cnt->task_id	=	_task_id;
	cnt->slave = _slave;
	cnt->last_send_status	= ~0; // for first send status msg

	if(SendToSlaveRfStartWriteFile(_slave->AddrRfSlave,GetNameFileBySlaveProtocol(_task_id),stat.size)){
		if (TaskSheduler_Exec(cnt, (sheduler_run_clbk_t)UpdateSlave_Task)){
			SetTimingByAddrClient(AddrRegistr,3000,5000,3000,40); // to do
			return true;
		}
	}
	
	free(cnt);
	return false;
}

#endif

void PROTO_FS_CLBK_NAME(EspFirmwareTaskReceived)(const PROTO_FS_MOD_PREFIX(EspFirmwareTask) *msg)
{
	FirmwareHeaderContext_T fw_header1, fw_header2;
	FirmwareHeader_Init(&fw_header1, g_cfg->flash_read_clbk, g_cfg->flash_write_clbk);
	FirmwareHeader_Init(&fw_header2, g_cfg->flash_read_clbk, g_cfg->flash_write_clbk);
  FirmwareHeader_Open(&fw_header1, FP_EXT_FLASH_FIRMWARE_1_HEADER_ADDR);
  FirmwareHeader_Open(&fw_header2, FP_EXT_FLASH_FIRMWARE_2_HEADER_ADDR);
	  
  FirmwareHeaderContext_T *update_hdr  = FirmwareHeader_SelectUpdate(&fw_header1, &fw_header2);
  FirmwareHeaderContext_T *default_hdr = FirmwareHeader_SelectDefault(&fw_header1, &fw_header2);

  bool is_success = false;
  if (FirmwareTaskUpdate == msg->task_id){
#ifndef ESP8266
		if(msg->dev_id == GetUuid32())
		{
#endif
			if(NULL != update_hdr){
				is_success = !(FirmwareHeader_GetFlags(update_hdr) & F_INSTALL_COMPLETED) &&
					(FirmwareHeader_GetFlags(update_hdr) & F_INSTALL_STARTED ||
						FirmwareHeader_SetFlags(update_hdr, F_INSTALL_STARTED));
#ifdef ESP8266
				if (is_success)
				{
					SetEspBootCommand(FirmwareHeader_GetFirmwareAddr(update_hdr), FirmwareHeader_GetSize(update_hdr));
					is_success = FirmwareHeader_SetFlags(update_hdr, F_INSTALL_COMPLETED);
				}
#endif
			}
#ifndef ESP8266
		}else{
			DescriptionSlave* slave = DescrSlaves_GetStrSlaveByDevId(msg->dev_id);
			if(slave != NULL) 
				is_success = UpdateSlave_SetTaskUpdate(slave,FirmwareTaskUpdate);
		}

#endif
	}
  else if (FirmwareTaskRollback == msg->task_id)
  {
#ifndef ESP8266
		if(msg->dev_id == GetUuid32())
		{
#endif
			if(NULL != update_hdr && NULL != default_hdr){
				is_success = !(FirmwareHeader_GetFlags(update_hdr) & F_ROLLBACK_COMPLETED) &&
					(FirmwareHeader_GetFlags(update_hdr) & F_ROLLBACK_STARTED ||
						FirmwareHeader_SetFlags(update_hdr, F_ROLLBACK_STARTED));
#ifdef ESP8266
				if (is_success)
				{
					SetEspBootCommand(FirmwareHeader_GetFirmwareAddr(default_hdr), FirmwareHeader_GetSize(default_hdr));
					is_success = FirmwareHeader_SetFlags(update_hdr, F_ROLLBACK_COMPLETED);
				}
#endif
			}
#ifndef ESP8266
		}else{
			// To do Add slave task roll back 
		}
#endif
  }
  else if (FirmwareTaskUpdateToDefault == msg->task_id)
  {
#ifndef ESP8266
		if(msg->dev_id == GetUuid32())
		{
#endif
			if(NULL != update_hdr && NULL != default_hdr){
				is_success = FirmwareHeader_SetFlags(update_hdr, F_DEFAULT_FIRMWARE) &&
					FirmwareHeader_SetFlags(default_hdr, F_DELETED_FIRMWARE);
			}
#ifndef ESP8266			
		}else{
		// To do Add slave task update to default 	
		}
#endif
  }
#ifndef ESP8266
	else if (FirmwareTaskInstallBootloader == msg->task_id)
	{
		if(msg->dev_id == GetUuid32()){
			spiffs_stat stat;
			is_success = SPIFFS_OK == SPIFFS_stat(FpFilesystem_GetFs(g_cfg->fs_cnt), BOOT_FIRMWARE_FILENAME, &stat);
			if (is_success)
			{
				if (SPIFFS_OK != SPIFFS_stat(FpFilesystem_GetFs(g_cfg->fs_cnt), INSTALL_BOOT_FILENAME, &stat))
					is_success = SPIFFS_OK == SPIFFS_creat(FpFilesystem_GetFs(g_cfg->fs_cnt), INSTALL_BOOT_FILENAME, 0);
			}
		}else{
			DescriptionSlave* slave = DescrSlaves_GetStrSlaveByDevId(msg->dev_id);
			if(slave != NULL) 
				is_success = UpdateSlave_SetTaskUpdate(slave,FirmwareTaskInstallBootloader);
		}
  }
#endif
	
	PROTO_FS_MOD_PREFIX(EspFirmwareTaskReply) reply = {0};
	reply.task_id = msg->task_id;
	reply.dev_id  = msg->dev_id;
	reply.status  = is_success ? FS_Status_Ok: FS_Status_Failed;
	TaskSheduler_CompleteOrPostpone(reply, PROTO_SEND_CLBK(EspFirmwareTaskReply), OnInstallFirmwareFailed, SEND_TIMEOUT_MSEC);
};


#ifndef ESP8266
bool FlashWriter_EraseClbk(uint32_t _page_addr, uint32_t _len)
{
	while (!InternalFlash_Unlock());
	bool is_success = InternalFlash_Erase(_page_addr, _len);
	InternalFlash_Lock();
	return is_success;
}

bool FlashWriter_WriteClbk(uint32_t _address, const uint8_t *_data, uint32_t _size)
{
	while (!InternalFlash_Unlock());
	bool is_success = InternalFlash_Write(_address, _data, _size);
	InternalFlash_Lock();
	return is_success;
}


void WriteBootloaderSize(const FlashWriterCfg_T *flash_writer, const uint32_t _size)
{
	uint32_t page_addr = BOOT_FIRMWARE_SIZE_ADDR & ~(flash_writer->page_size - 1);
	if (flash_writer->cleared_page_addr != page_addr && FlashWriter_EraseClbk(page_addr, FLASH_PAGE_SIZE))
		FlashWriter_WriteClbk(BOOT_FIRMWARE_SIZE_ADDR, (const uint8_t *)_size, sizeof(_size));
}


void TryUpdateBootloader(void)
{
	spiffs_stat stat;
	spiffs_file fh;
	
	if (SPIFFS_OK != SPIFFS_stat(FpFilesystem_GetFs(g_cfg->fs_cnt), INSTALL_BOOT_FILENAME, &stat))
		return;
	fh = SPIFFS_open_by_page(FpFilesystem_GetFs(g_cfg->fs_cnt), stat.pix, SPIFFS_RDWR, 0);
	if (0 >= fh || SPIFFS_OK != SPIFFS_fremove(FpFilesystem_GetFs(g_cfg->fs_cnt), fh))
		return;
	
	if (SPIFFS_OK != SPIFFS_stat(FpFilesystem_GetFs(g_cfg->fs_cnt), BOOT_FIRMWARE_FILENAME, &stat))
		return;
	fh = SPIFFS_open_by_page(FpFilesystem_GetFs(g_cfg->fs_cnt), stat.pix, SPIFFS_RDWR, 0);
	if (0 >= fh)
		return;	
	
	FlashWriterCfg_T flash_writer;
	if (!FlashWriter_Begin(&flash_writer, FLASH_PAGE_SIZE, BOOT_FIRMWARE_ADDR, BOOT_FIRMWARE_MAX_SIZE, FlashWriter_EraseClbk, FlashWriter_WriteClbk))
	{
		SPIFFS_close(FpFilesystem_GetFs(g_cfg->fs_cnt), fh);
		return;
	}
	
	int readed;
	uint8_t buf[512];
	do 
	{
		readed = SPIFFS_read(FpFilesystem_GetFs(g_cfg->fs_cnt), fh, buf, sizeof(buf));
		if (0 > readed)	
		{
			SPIFFS_close(FpFilesystem_GetFs(g_cfg->fs_cnt), fh);
			return;
		}
		
		if (!FlashWriter_Write(&flash_writer, buf, readed))
		{
			SPIFFS_close(FpFilesystem_GetFs(g_cfg->fs_cnt), fh);
			return;
		}
	} 
	while(sizeof(buf) == readed);
	
	WriteBootloaderSize(&flash_writer, flash_writer.written);
	
	SPIFFS_fremove(FpFilesystem_GetFs(g_cfg->fs_cnt), fh);
		
	__DSB();
	__ISB();
}

#endif

//-----------------------------------------------------------------------------------------------------------

void ResetProtoFs(void)
{
  TaskSheduler_RemoveAll(LsDirTask,    CloseLsDirContext);
  TaskSheduler_RemoveAll(ReadFileTask, CloseReadFileContext);
	TaskSheduler_RemoveAll(CalcMd5Task,  CloseCalcMd5Context);
  CloseWriteContext(&g_cfg->writeFileContext, &g_cfg->writeFileTask);
}
