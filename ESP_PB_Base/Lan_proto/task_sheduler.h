//-----------------------------------------------------------------//
//  Developed by Vladimir Sosedkin for Forpost(c) company in 2018  //
//  email: sosed.mail@gmail.com                                    //
//-----------------------------------------------------------------//

#pragma once
#ifndef TASK_SHEDULER_H
#define TASK_SHEDULER_H

#include <stdbool.h>
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

typedef bool (*sheduler_run_clbk_t)   (void *_context);
typedef void (*time_expired_clbk_t)   (void *_context);
typedef void (*context_handler_clbk_t)(void *_context);
typedef void (*fail_clbk_t)           (void *_context);

bool TaskSheduler_Exec(void *_context, sheduler_run_clbk_t _clbk);

bool TaskSheduler_IsRunning(const void *_context, const sheduler_run_clbk_t _clbk);

bool TaskSheduler_IsCallback(const sheduler_run_clbk_t _clbk);

void TaskSheduler_Update(void);

bool TaskSheduler_ExecTimeout(
		void                *_task_context, 
		sheduler_run_clbk_t  _clbk, 
		time_expired_clbk_t  _handle_expired, 
		const uint32_t       _timeout);

void TaskSheduler_CompleteOrPostponeImpl(
		void               *_task_context, 
		uint32_t            _task_context_size, 
		sheduler_run_clbk_t _task_clbk, 
		fail_clbk_t         _fail_clbk, 
		const uint32_t      _timeout);
		
bool TaskSheduler_Remove(const void *_context, const sheduler_run_clbk_t _clbk);

unsigned TaskSheduler_RemoveAll(sheduler_run_clbk_t _clbk, context_handler_clbk_t _handle_context);

#ifdef __cplusplus
}
#endif
	
	
#ifdef __cplusplus
template<typename T1, typename T2>
inline void TaskSheduler_CompleteOrPostpone(
    T1             &_cnt, 
		bool           (*_task_clbk)(T2 *),
		fail_clbk_t    _fail_clbk, 
		const uint32_t _timeout)
{
	TaskSheduler_CompleteOrPostponeImpl(&_cnt, sizeof(T1), reinterpret_cast<sheduler_run_clbk_t>(_task_clbk), _fail_clbk, _timeout);
}
#else //!__cplusplus	
#define TaskSheduler_CompleteOrPostpone(_cnt, _task_clbk, _fail_clbk, _timeout) \
	TaskSheduler_CompleteOrPostponeImpl(&(_cnt), sizeof(_cnt), (sheduler_run_clbk_t)(_task_clbk), (fail_clbk_t)(_fail_clbk), (_timeout))
#endif //__cplusplus
	

#endif // TASK_SHEDULER_H
