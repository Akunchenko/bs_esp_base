#include "server_discovery.h"
#include "task_sheduler.h"

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

// #TODO Server discovare has conflict with sending logs via udp. Now, I don't know why.
//#include "runtime_dbg.h"
//#define RTDB_Log(...)

namespace serv_dis
{
  namespace
  {
    const uint64_t UDP_RECV_DATAGRAM_PREFIX = 0xc94222115fc942d3;
    const uint64_t UDP_RESP_DATAGRAM_PREFIX = 0xade7e97f3f51477b;

    const unsigned SEND_RECV_PERIOD_MSEC = 100;
    const unsigned START_PORT  = 1024;
    const unsigned PORTS_COUNT = 500;

    const string empty = { 0 };
    const auto IS_SUCCESS = 1;

    typedef IPAddress Ipv4Addr_T;
    typedef uint16_t  Ipv4Port_T;
    typedef uint32_t  ServerId_T;
    typedef uint8_t   Priority_T;
    typedef uint16_t  NameLen_T;
    typedef string    Name_T;
    
    const unsigned MIN_UDP_RESP_DATAGRAM_SIZE = 
      sizeof(UDP_RESP_DATAGRAM_PREFIX) + 
      sizeof(Ipv4Addr_T) + 
      sizeof(Ipv4Port_T) + 
      sizeof(ServerId_T) +
      sizeof(Priority_T) + 
      sizeof(NameLen_T);

    static const unsigned RESPONCE_TIMEOUT_MSEC = 3000;

    WiFiUDP g_udp;

    server_discovery_clbk_ptr_t g_responce_clbk = nullptr;

    struct DiscoveryContext
    {
      uint32_t  timestamp;
      IPAddress broadcast_addr;
      uint8_t   dev_type; 
      string    serial_num;
      uint16_t  idx;

      DiscoveryContext(const IPAddress &_addr, const uint8_t _dev_type, const string &_serial_num):
        idx(0),
        timestamp(0),
        broadcast_addr(_addr),
        dev_type(_dev_type)
      {
        serial_num.ptr = new char[_serial_num.len];
        memcpy(serial_num.ptr, _serial_num.ptr, _serial_num.len);
        serial_num.len = _serial_num.len;
      }

      ~DiscoveryContext()
      {
        delete[] serial_num.ptr;
      }
    };

    bool SendDiscoveryPacketsTask(DiscoveryContext *_ct)
    {
      if (SEND_RECV_PERIOD_MSEC > millis() - _ct->timestamp)
        return false;

      if (IS_SUCCESS != g_udp.beginPacket(_ct->broadcast_addr, SERVER_ANN_PORT_START + _ct->idx))
      {
        g_udp.stop();
        g_responce_clbk(IPAddress(), 0, 0, 0, empty);
        return true;
      }

      g_udp.write(reinterpret_cast<const char *>(&UDP_RECV_DATAGRAM_PREFIX), sizeof(UDP_RECV_DATAGRAM_PREFIX));
      g_udp.write(reinterpret_cast<const char *>(_ct->serial_num.ptr), _ct->serial_num.len);
      g_udp.write(_ct->dev_type);

      if (IS_SUCCESS != g_udp.endPacket())
      {
        g_udp.stop();
       // RTDB_Log(LOG_TYPE_INFO, "\n Fail send discover msg; broadcast_addr:%s\n", _ct->broadcast_addr.toString().c_str());
        Serial.printf("Fail send discover msg; broadcast_addr: % s\n", _ct->broadcast_addr.toString().c_str());
        g_responce_clbk(IPAddress(), 0, 0, 0, empty);
        return true;
      }
      else
      {
        //RTDB_Log(LOG_TYPE_INFO, "Send to ann port %d\n", SERVER_ANN_PORT_START + _ct->idx);
		  Serial.printf("Send to ann port %d\n", SERVER_ANN_PORT_START + _ct->idx);
      }
      
      if (SERVER_ANN_PORT_COUNT > ++_ct->idx)
      {
        _ct->timestamp = millis();
        return false;
      }

      return true;
    }

    bool ReadServersResponceTask(DiscoveryContext *_ct)
    {
		//Serial.printf("ReadServersResponceTask\n");
      if (RESPONCE_TIMEOUT_MSEC < millis() - _ct->timestamp)
      {
        g_udp.stop();
        delete _ct;
        g_responce_clbk(IPAddress(), 0, 0, 0, empty);
        //RTDB_Log(LOG_TYPE_INFO, "Discover server complete search!\n");
        Serial.printf("Discover server complete search!\r\n");
        return true;
      }

      int udpPacketSize = g_udp.parsePacket();
      if (udpPacketSize == 0)
        return false;

      if (MIN_UDP_RESP_DATAGRAM_SIZE > udpPacketSize)
      {
       // RTDB_Log(LOG_TYPE_CRIT, "1 Wrong announcement message header from %s:%d size=%d/%d\n", g_udp.remoteIP().toString().c_str(), g_udp.remotePort(), MIN_UDP_RESP_DATAGRAM_SIZE, udpPacketSize);
        return false;
      }

      uint64_t ann_prefix;
      g_udp.read(reinterpret_cast<char *>(&ann_prefix), sizeof(ann_prefix));
      if (0 != memcmp(&ann_prefix, &UDP_RESP_DATAGRAM_PREFIX, sizeof(ann_prefix)))
      {
        //RTDB_Log(LOG_TYPE_CRIT, "2 Wrong announcement message header from %s : %d\n", g_udp.remoteIP().toString().c_str(), g_udp.remotePort());
        return false;
      }

      uint32_t addr;
      g_udp.read(reinterpret_cast<char *>(&addr), sizeof(addr));
      Ipv4Addr_T ipv4_addr(addr);
      if (0 != addr)
      {
        std::swap(ipv4_addr[0], ipv4_addr[3]);
        std::swap(ipv4_addr[1], ipv4_addr[2]);
      }
      else
        ipv4_addr = g_udp.remoteIP();

      Ipv4Port_T ipv4_port;
      g_udp.read(reinterpret_cast<char *>(&ipv4_port), sizeof(ipv4_port));

      ServerId_T server_id;
      g_udp.read(reinterpret_cast<char *>(&server_id), sizeof(server_id));

      Priority_T priority;
      g_udp.read(reinterpret_cast<char *>(&priority), sizeof(priority));

      uint16_t name_len;
      g_udp.read(reinterpret_cast<char *>(&name_len), sizeof(name_len));
      string name;
      name.len = name_len;
      //RTDB_Log(LOG_TYPE_INFO, "Name len = %d\n", name_len);
      name.ptr = new char[name_len + 1];
      *(name.ptr + name_len) = 0;
      g_udp.read(name.ptr, name_len);

      g_responce_clbk(ipv4_addr, ipv4_port, server_id, priority, name);

     // RTDB_Log(LOG_TYPE_INFO, "Discovered server name=\"%s\" ip=%s at port=%d\n", name.ptr, ipv4_addr.toString().c_str(), ipv4_port);
	  Serial.printf("Discovered server name=\"%s\" ip=%s at port=%d\n", name.ptr, ipv4_addr.toString().c_str(), ipv4_port);

      return false;
    }

    bool SearchFreeUdpPortTask(DiscoveryContext *_ct)
    {
     // RTDB_Log(LOG_TYPE_INFO, "Try open discovery service port at %d\n", _ct->idx);
		Serial.printf("Try open discovery service port at %d\n", _ct->idx);
		if (IS_SUCCESS != g_udp.begin(_ct->idx))
      {
        if (START_PORT + PORTS_COUNT > ++_ct->idx)
          return false;

        delete _ct;
        g_responce_clbk(IPAddress(), 0, 0, 0, empty);
       // RTDB_Log(LOG_TYPE_CRIT, "Can't open discovery service port!\n");
		Serial.printf("Can't open discovery service port!\n");
        return true;
      }

     // RTDB_Log(LOG_TYPE_INFO, "Discovery service started at %d port.\n", _ct->idx);
		Serial.printf("Discovery service started at %d port.\n", _ct->idx);

      _ct->timestamp = millis();
      _ct->idx       = 0;
      if (!TaskSheduler_Exec(_ct, reinterpret_cast<sheduler_run_clbk_t>(SendDiscoveryPacketsTask)) ||
          !TaskSheduler_Exec(_ct, reinterpret_cast<sheduler_run_clbk_t>(ReadServersResponceTask)))
      {
        g_udp.stop();
        TaskSheduler_Remove(_ct, reinterpret_cast<sheduler_run_clbk_t>(SendDiscoveryPacketsTask));
        delete _ct;
      }

      return true;
    }
  }


  void init(server_discovery_clbk_ptr_t _clbk_ptr)
  {
    g_responce_clbk = _clbk_ptr;
  }


  void askServersToAnnounce(const uint8_t _dev_type, const string _serial_num)
  {
    IPAddress broadcast_addr = uint32_t(WiFi.localIP()) | ~uint32_t(WiFi.subnetMask());
    DiscoveryContext* ct = new DiscoveryContext(broadcast_addr, _dev_type, _serial_num);
      
    if (nullptr != ct)
    {
      ct->idx      = START_PORT;
      ct->dev_type = _dev_type;
      if (TaskSheduler_Exec(ct, reinterpret_cast<sheduler_run_clbk_t>(SearchFreeUdpPortTask)))
      {
       // RTDB_Log(LOG_TYPE_INFO, "Send announces messages by broadcast_addr:%s\n", broadcast_addr.toString().c_str());
		Serial.printf("Send announces messages by broadcast_addr:%s\n", broadcast_addr.toString().c_str());
		//LogBuff_AddEvent(FIND_SERVER, 1);
      }
      else
      {
        delete ct;
       // RTDB_Log(LOG_TYPE_CRIT, "Can't run announce task!\n");
		Serial.printf("Can't run announce task!\n");
      }
    }
  }
  void askServersToAnnounce(const uint8_t _dev_type, const string _serial_num, const IPAddress& _ip_addr)
  {
      IPAddress broadcast_addr = _ip_addr;
      //Serial.printf("Send announces messages to custom IP_ADDRES:%s\n", _ip_addr.toString().c_str())  ;
      
      DiscoveryContext* ct = new DiscoveryContext(broadcast_addr, _dev_type, _serial_num);

      if (nullptr != ct)
      {
          ct->idx = START_PORT;
          ct->dev_type = _dev_type;
          if (TaskSheduler_Exec(ct, reinterpret_cast<sheduler_run_clbk_t>(SearchFreeUdpPortTask)))
          {
              // RTDB_Log(LOG_TYPE_INFO, "Send announces messages by broadcast_addr:%s\n", broadcast_addr.toString().c_str());
              Serial.printf("Send announces messages to custom IP_ADDRES:%s\n", broadcast_addr.toString().c_str());
          }
          else
          {
              delete ct;
              // RTDB_Log(LOG_TYPE_CRIT, "Can't run announce task!\n");
              Serial.printf("Can't run announce task!\n");
          }
      }
  }

} // namespace serv_dis