#include "callbacs.h"
#include "Arduino.h"
#include "Indication.h"
#include "esp.pb.h"
#include <Lan_proto\server_discovery.h>
#include "wifi_module.h"
char version[5] = { '1','0','2' };
char firmware_ver[6];
uint8_t uniqueId[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
uint8_t cardId[8] = { 8,7,6,5,4,3,2,1 };

void OnConnectionEstablished() {
	
    HelloFromHub hello = { 0 };
    hello.was_early_connected = true;
    hello.SerialNumber = ESP.getChipId();
    hello.firmware_ver.len = sizeof version;
    hello.firmware_ver.ptr = version;
   
    SendHelloFromHub(&hello);

    indication.ConnectedToServer();
} 
void SendLikePing(void) {
    static uint32_t pingTimeout = 0;
   
    if (millis() >= pingTimeout) {
        SendPing();
        pingTimeout = millis() + 30000;
    }
}

void OnConnectionClosed()
{
    indication.DisconnectFromServer();
    char serial[] = "12345";
    string serial_str;
    serial_str.ptr = serial;
    serial_str.len = strlen(serial);
    WFM_SetConnStateApMode(0);
    if (WFM_GetPartalState()) {
        serv_dis::askServersToAnnounce(1, serial_str, WFM_GetCustomIpAddres()); // Not Found Server
    }
    else
    {
        serv_dis::askServersToAnnounce(1, serial_str);

    }
}

size_t availableForWrite()
{
    return 128 < CBufGetFreeSize(esp_proto->sendBuffer) ?
        CBufGetFreeSize(esp_proto->sendBuffer) - 128 : 0;
}

bool OnFullCmdReceived(MemStream* _stream) {
    const size_t send_space = availableForWrite();
    const size_t data_size = GET_SIZE(_stream);
    if (data_size > send_space)
    {
        //#TODO maybe add quiet error for future tunning
        return false;
    }

    size_t wrote_size = 0;
    while (wrote_size != data_size)
    {
        size_t size = GET_CONTINUOUS_SIZE(_stream);
        void* buf; size_t bufsize = 0;
        CBufGetBufForWrite(esp_proto->sendBuffer, &buf, &bufsize);
        size = min(size, bufsize);
        memcpy(buf, GET_PTR(_stream), size);
        CBufMarkAsWritten(esp_proto->sendBuffer, size);
        INC_POS_BY(_stream, size);
        wrote_size += size;
    }

    //RTDB_Log(LOG_TYPE_INFO, "Sent SRV->STM chunk: %d bytes\n", wrote_size);

    // #TODO If wrote_size != data_size it means that we send broken data to stm. Now I don't know what to do
    return wrote_size == data_size;
}

