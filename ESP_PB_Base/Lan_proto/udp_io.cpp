#include "udp_io.h"
#include "tinyfsm.hpp"
#include <Stream.h>
#include <WiFiUdp.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "cbuffer.h"
#include <limits>
#include "esp.pb.h"
#include "server_discovery.h"
#include "wifi_module.h"

namespace udp_io
{
  const unsigned ESP_SRV_RANGE_FIRST = 0;
  const unsigned ESP_SRV_RANGE_LAST  = 1099;

  static const unsigned long SEND_TIMEOUT_MSEC = 100;
  static const size_t RESERVED_BUFF_SIZE       = 64;

  static const size_t MAX_UNCONFIRMED_RECEIVED_DATA = 256;

  static const auto SENDING_UPD_PACKET_DELAY_MSEC = 10;

  typedef uint16_t counter_t;

  namespace
  {
    // Variables

    WiFiUDP  g_udp;
    uint16_t g_udp_port = 1024; // first if user or registered ports

    IPAddress g_serv_ip;
    uint16_t  g_serv_port = 0;

    counter_t g_send_seq   = 0;
    counter_t g_recv_seq = 0;
    counter_t g_confirmed_recv_seq = 0;

    on_connected_clbk_ptr_t    g_conn_clbk; 
    on_closed_clbk_ptr_t       g_clos_clbk;
    on_cmd_received_clbk_ptr_t g_cmd_rec_clbk_ptr;

	
	IPAddress server_ip;
	uint16_t server_port;
	uint32_t server_id;
  }

  struct UdpIOPacket
  {
    counter_t send_seq;
    counter_t recv_seq;
    uint8_t   flags;
    Stream    *istream;

    static const counter_t UDP_PACKET_HEADER_SIZE =
      sizeof(UdpIOPacket::send_seq)   +
      sizeof(UdpIOPacket::recv_seq) +
      sizeof(UdpIOPacket::flags);

    static const counter_t MAX_UDP_PACKET_SIZE = 512;

    static const counter_t MAX_UDP_PACKET_DATA_SIZE = MAX_UDP_PACKET_SIZE - UDP_PACKET_HEADER_SIZE;

    bool readFrom(Stream &_stream)
    {
      if (UdpIOPacket::MAX_UDP_PACKET_SIZE < _stream.available() || 
          UdpIOPacket::UDP_PACKET_HEADER_SIZE > _stream.available())
        return false;

      if (sizeof(send_seq)   != _stream.readBytes(reinterpret_cast<char *>(&send_seq),   sizeof(send_seq))   ||
          sizeof(recv_seq) != _stream.readBytes(reinterpret_cast<char *>(&recv_seq), sizeof(recv_seq)) ||
          sizeof(flags)          != _stream.readBytes(reinterpret_cast<char *>(&flags),          sizeof(flags)))
        return false;

      istream = &_stream;

      return true;
    }

	bool sendTo(Stream &_stream) const
    {
      if (sizeof(send_seq) != _stream.write(reinterpret_cast<const char *>(&send_seq), sizeof(send_seq)) ||
        sizeof(recv_seq) != _stream.write(reinterpret_cast<const char *>(&recv_seq), sizeof(recv_seq)) ||
        sizeof(flags)          != _stream.write(reinterpret_cast<const char *>(&flags), sizeof(flags)))
        return false;

      if (nullptr == istream)
        return true;

      for (unsigned i = 0; i < MAX_UDP_PACKET_DATA_SIZE; ++i)
      {
        int val = istream->read();
        if (-1 == val)
          break;
        
        if(0 ==_stream.write(val))
          return false;
      }

      return true;
    }
  };

  class InputMemStream : public Stream
  {
    MemStream m_mstream;

  public:
    InputMemStream(const MemStream &_ms) : m_mstream(_ms) {}

    InputMemStream(CyclicBuffer *_cb) : m_mstream({0})
    { 
      CBufGetStreamForRead(_cb, &m_mstream);
    }

    size_t write(uint8_t) override { return 0; }

    int available() override { return GET_SIZE(&m_mstream); }

    int read() override 
    { 
      if (0 == GET_SIZE(&m_mstream)) 
        return -1;

      auto value = *GET_PTR(&m_mstream);
      INC_POS_BY(&m_mstream, 1);
      return value;
    }

    int peek() override 
    { 
      if (0 == GET_SIZE(&m_mstream))
        return -1;

      return *GET_PTR(&m_mstream);
    }

    size_t readBytes(char *buffer, size_t length) override
    {
      size_t data_size   = std::min(length, GET_SIZE(&m_mstream));
      size_t readed_size = 0;
      while (readed_size != data_size)
      {
        size_t chunk_size = std::min(data_size, GET_CONTINUOUS_SIZE(&m_mstream));
        memcpy(buffer, GET_PTR(&m_mstream), chunk_size);
        readed_size += chunk_size;
        INC_POS_BY(&m_mstream, chunk_size);
      }

      return data_size;
    }
  };


  bool point_in_range(const uint16_t _start, const uint16_t _len, const uint16_t pos)
  {
    return (0xFFFF & (pos - _start)) <= _len;
  }


  bool sendSynPacket(const IPAddress _ip, const uint16_t _port)
  {
    if (!g_udp.beginPacket(_ip, _port))
      return false;

    UdpIOPacket packet  = { 0 };
    packet.flags        = F_SYN;
    packet.send_seq = g_send_seq;

    bool is_successful = packet.sendTo(g_udp) && g_udp.endPacket();
		if (is_successful)
		{
			delay(SENDING_UPD_PACKET_DELAY_MSEC);
			//RTDB_Log(LOG_TYPE_INFO, "Send SYN Packet packet.send_seq=%d\n", packet.send_seq);
			Serial.printf("Send SYN Packet packet.send_seq=%d\n", packet.send_seq);
		}
		else
			//RTDB_Log(LOG_TYPE_WARN, "Send SYN Packet packet failed!\n");
			Serial.printf("Send SYN Packet packet failed!\n");
    return is_successful;
  }

  bool sendResetPacket(const IPAddress _ip, const uint16_t _port, const uint16_t _send_seq, const uint16_t _recv_seq)
  {
    if (!g_udp.beginPacket(_ip, _port))
      return false;

    UdpIOPacket packet = { 0 };
	packet.recv_seq = _recv_seq;
	packet.send_seq = _send_seq;
	packet.flags = F_RST;

    bool is_successful = packet.sendTo(g_udp) && g_udp.endPacket();
    if (is_successful)
      delay(SENDING_UPD_PACKET_DELAY_MSEC);

    return is_successful;
  }

  bool sendDataPacket(const IPAddress _ip, const uint16_t _port)
  {
    if (!g_udp.beginPacket(_ip, _port))
      return false;

    UdpIOPacket packet = { 0 };
    InputMemStream istream(esp_proto->sendBuffer);
    packet.istream        = &istream;
    packet.flags          = F_DAT;
    packet.send_seq  = g_send_seq;
    packet.recv_seq = g_recv_seq;
    
    bool is_successful = packet.sendTo(g_udp) && g_udp.endPacket();
	if (is_successful)
		g_confirmed_recv_seq = g_recv_seq;

	//  TODO: Alexander: find out why we need this delay
	if (is_successful)
      delay(SENDING_UPD_PACKET_DELAY_MSEC);

    return is_successful;
  }


  // -------------- Events ---------------------------------------------------

  struct UpdateEvent_T : tinyfsm::Event
  {
    unsigned long time;
  };

  struct EspConnectToTcpEvent_T : tinyfsm::Event
  {
    IPAddress ip_addr;
    uint16_t  port;
  };

  struct UdpIOPacketEvent_T : tinyfsm::Event
  {
    IPAddress   remote_ip;
    uint16_t    remote_port;
    UdpIOPacket *packet;
  };


  // ------------ State Machine -----------------------------------------

  class UdpIOFSM_T : public tinyfsm::Fsm<UdpIOFSM_T>
  {
  public:
    /* default reaction for unhandled events */
    void react(tinyfsm::Event const &) {};

    virtual void react(UpdateEvent_T const &_event) {};

    virtual void react(EspConnectToTcpEvent_T const &_event) {};

    virtual void react(UdpIOPacketEvent_T const &_event) {};

    virtual void entry() {};

    virtual void exit() {};
  };

  using fsm = UdpIOFSM_T;


  // ------------------ States declaration -----------------------------------

  class ConnStateClosed_T : public UdpIOFSM_T
  {
  public:
    void react(EspConnectToTcpEvent_T const &_event) override;

    void react(UdpIOPacketEvent_T const &_event) override;

    void entry() override;
  };


  class ConnStateStage1_T : public UdpIOFSM_T
  {
    static const unsigned      MAX_PROBES_COUNT = 100;
    static const unsigned long PROBE_TIMEOUT    = 300;

  public:
    void react(UpdateEvent_T const &_event) override;

    void react(EspConnectToTcpEvent_T const &_event) override;

    void react(UdpIOPacketEvent_T const &_event) override;

    void entry() override;

  private:
    unsigned      m_probes_count;
    unsigned long m_timestamp;
  };


  class ConnStateEstablished_T : public UdpIOFSM_T
  {
  public:
    void react(UpdateEvent_T const &_event) override;

    void react(EspConnectToTcpEvent_T const &_event) override;

    void react(UdpIOPacketEvent_T const &_event) override;

    void entry() override;

    void exit() override;

  private:
    void SendDataPacket();

    unsigned long m_timestamp;
  };


  //-------- class ConnStateClosed_T

  void ConnStateClosed_T::react(EspConnectToTcpEvent_T const &_event)
  {
    if (0 == _event.port)
      return;

    g_serv_ip   = _event.ip_addr;
    g_serv_port = _event.port;

    transit<ConnStateStage1_T>();
  }

  void ConnStateClosed_T::react(UdpIOPacketEvent_T const &_event)
  {
    if (_event.packet->flags & F_RST)
      return;

    sendResetPacket(_event.remote_ip, _event.remote_port, _event.packet->recv_seq, _event.packet->send_seq);
  }

  void ConnStateClosed_T::entry()
  {
    if (g_serv_port > 0)
    {
      sendResetPacket(g_serv_ip, g_serv_port, g_send_seq, g_recv_seq);
      g_serv_ip   = IPAddress();
      //g_serv_ip = WiFi.localIP();
      Serial.printf("g_serv_ip %s\n", WiFi.localIP().toString().c_str());
      g_serv_port = 0;
    }

    //RTDB_Log(LOG_TYPE_INFO, "------- Go into ConnStateClosed_T state\n");
	Serial.printf("------- Go into ConnStateClosed_T state\n");
	//LogBuff_AddEvent(UDP_EVENT, UIO_ST_CLOSED);
  }


  //-------- class ConnStateStage1_T : public UdpIOFSM_T

  void ConnStateStage1_T::react(UpdateEvent_T const &_event)
  {
    if (PROBE_TIMEOUT > _event.time - m_timestamp)
      return;

    if (++m_probes_count >= MAX_PROBES_COUNT)
    {
     // RTDB_Log(LOG_TYPE_INFO, "ConnStateStage reach MAX_PROBES_COUNT\n");
	  //Serial.printf("ConnStateStage reach MAX_PROBES_COUNT\n");
      transit<ConnStateClosed_T>();
      return;
    }

    m_timestamp = millis();
    sendSynPacket(g_serv_ip, g_serv_port);
	//Serial.printf("ConnStateStage1_T::UpdateEvent_T\n");
  }

  void ConnStateStage1_T::react(EspConnectToTcpEvent_T const &_event)
  {
    transit<ConnStateClosed_T>();

    if (0 == _event.port)
      return;

    g_serv_ip   = _event.ip_addr;
    g_serv_port = _event.port;

    transit<ConnStateStage1_T>();
	//Serial.printf("ConnStateStage1_T::EspConnectToTcpEvent_T\n");
  }

  void ConnStateStage1_T::react(UdpIOPacketEvent_T const &_event)
  {
    if (g_serv_ip                     != _event.remote_ip   || 
        g_serv_port                   != _event.remote_port ||
        _event.packet->recv_seq != g_send_seq     ||
        !(_event.packet->flags & F_SYN))
    {
      return;
    }

    if (_event.packet->flags & F_ACK)
    {
      g_recv_seq      = _event.packet->send_seq;
      g_confirmed_recv_seq = _event.packet->send_seq;
      //RTDB_Log(LOG_TYPE_INFO, "ConnStateStage1_T: set g_recv_seq = %d\n", g_recv_seq);
	  //Serial.printf("ConnStateStage1_T: set g_recv_seq = %d\n", g_recv_seq);
      transit<ConnStateEstablished_T>();
      fsm::dispatch(_event);
    }
    else if (_event.packet->flags & F_RST)
    {
    /*  RTDB_Log(
        LOG_TYPE_INFO, 
        "ConnStateStage1_T: server refused connection, flags=%d, send_seq=%d, recv_seq=%d\n", 
        _event.packet->flags, 
        _event.packet->send_seq, 
        _event.packet->recv_seq
      );*/
	  Serial.printf("ConnStateStage1_T: server refused connection, flags=%d, send_seq=%d, recv_seq=%d\n",
		  _event.packet->flags,
		  _event.packet->send_seq,
		  _event.packet->recv_seq);
      transit<ConnStateClosed_T>();
    }
	//Serial.printf("ConnStateStage1_T::UdpIOPacketEvent_T\n");
  }

  void ConnStateStage1_T::entry()
  {
    //RTDB_Log(LOG_TYPE_INFO, "------- Go into ConnStateStage1_T state, send_seq=%d\n", g_send_seq);
	Serial.printf("------- Go into ConnStateStage1_T state, send_seq=%d\n", g_send_seq);
	//LogBuff_AddEvent(UDP_EVENT, UIO_ST_STAGE1);
	// simply some initial value for security
    g_send_seq = random(1, std::numeric_limits<counter_t>::max());

    // send ACK message packet immediately at next UpdateEvent_T
    m_timestamp    = 0;
    m_probes_count = 0;
  }


  //-------- class ConnStateEstablished_T

  void ConnStateEstablished_T::react(UpdateEvent_T const &_event)
  {
	// Alexander: we need to send packet if 
	//    1) sendBuffer has data to send
	// or 2) we received new data and need to send confirmation (acknowledgement) back to other side
      if (SEND_TIMEOUT_MSEC < millis() - m_timestamp && (!CBufIsEmpty(esp_proto->sendBuffer) || g_confirmed_recv_seq != g_recv_seq)) {
          Serial.printf("Sent=%d\n", CBufGetDataSize(esp_proto->sendBuffer));
          SendDataPacket();
      }
       /* if (SEND_TIMEOUT_MSEC < millis() - m_timestamp) {
            Serial.printf("Sent=%d\n", CBufGetDataSize(esp_proto->sendBuffer));
            SendDataPacket();
        }*/
	//Serial.printf("ConnStateEstablished_T::UpdateEvent_T\n");
  }

  void ConnStateEstablished_T::react(EspConnectToTcpEvent_T const &_event)
  {
    //RTDB_Log(LOG_TYPE_INFO, "ConnStateEstablished_T: EspConnectToTcpEvent_T\n");
    transit<ConnStateClosed_T>();

    if (0 == _event.port)
      return;

    g_serv_ip = _event.ip_addr;
    g_serv_port = _event.port;

    transit<ConnStateStage1_T>();
	//Serial.printf("ConnStateEstablished_T::EspConnectToTcpEvent_T\n");
  }

  void ConnStateEstablished_T::react(UdpIOPacketEvent_T const &_event)
  {
    if (g_serv_ip != _event.remote_ip || g_serv_port != _event.remote_port)
      return;

    if (_event.packet->flags & F_DAT)
    {
      if (point_in_range(g_send_seq, CBufGetDataSize(esp_proto->sendBuffer), _event.packet->recv_seq))
      {
        uint16_t confirmed = 0xFFFF & (_event.packet->recv_seq - g_send_seq);
        CBufMarkAsRead(esp_proto->sendBuffer, confirmed);
        g_send_seq = _event.packet->recv_seq;

        if (CBufIsEmpty(esp_proto->sendBuffer))
          m_timestamp = 0;

		//Serial.printf("Confirmed=%d bytes now g_send_seq=%d\n", confirmed, g_send_seq);
		//SendLogEn = true;
		//LogBuff_AddEvent(UDP_WRITE, g_send_seq << 16 + confirmed);
      }

      Stream *is     = _event.packet->istream;
      unsigned avail = is->available();
      if (0 < avail && point_in_range(_event.packet->send_seq, avail, g_recv_seq))
      {
        uint16_t offset = 0xFFFF & (g_recv_seq - _event.packet->send_seq);
        if (offset < avail)
        {
          while (0 != offset--)
            is->read();


		  // TODO:  Alexander: read data by bigger blocks (not by bytes)
		  uint16_t wrote = 0;
          int val = is->read();
          while (-1 != val && CBufWriteByte(esp_proto->recvBuffer, val))
          {
            ++wrote;
            val = is->read();
          }

          if (wrote != 0)
          {
            g_recv_seq = 0xFFFF & (g_recv_seq + wrote);

           /* if (g_received_bytes - g_received_bytes_sent > MAX_UNCONFIRMED_RECEIVED_DATA)
              SendDataPacket();*/
          }

          //RTDB_Log(LOG_TYPE_INFO, "Received=%d bytes now g_received_bytes=%d\n", wrote, g_received_bytes);
		 // Serial.printf("Received=%d bytes now g_received_bytes=%d\n", wrote, g_recv_seq);
		  //LogBuff_AddEvent(UDP_READ, g_recv_seq << 16 + wrote);
        }
      }
    }

    if (_event.packet->flags & F_RST)
    {
      //RTDB_Log(LOG_TYPE_INFO, "ConnStateEstablished_T: _event.packet->flags & F_RST\n");
	  Serial.printf("ConnStateEstablished_T: _event.packet->flags & F_RST\n");
      transit<ConnStateClosed_T>();
    }
	//Serial.printf("ConnStateEstablished_T::UdpIOPacketEvent_T\n");
  }

  void ConnStateEstablished_T::entry()
  {
    //RTDB_Log(LOG_TYPE_INFO, "------- Go into ConnStateEstablished_T state\n");
	Serial.printf("------- Go into ConnStateEstablished_T state\n");
	//LogBuff_AddEvent(UDP_EVENT, UIO_ST_ESTABL);
	m_timestamp = 0;

    CBufClear(esp_proto->recvBuffer);
    CBufClear(esp_proto->sendBuffer);

    g_conn_clbk();
    SendDataPacket();
  }

  void ConnStateEstablished_T::exit()
  {
    //RTDB_Log(LOG_TYPE_INFO, "Exit form ConnStateEstablished_T state\n");
	Serial.printf("Exit form ConnStateEstablished_T state\n");
    g_clos_clbk();
  }

  void ConnStateEstablished_T::SendDataPacket()
  {
     /* Serial.printf("SendDataPacket:\n");
      Serial.printf("g_serv_ip %s\n", WiFi.localIP().toString().c_str());
      Serial.printf("g_serv_port %d\n", g_serv_port);*/
      if (sendDataPacket(g_serv_ip, g_serv_port))
    {
      m_timestamp = millis();
     }
  }

  // ---------------- Implementation -------------------------------------------------
  bool WriteToUdpSendBuffer(MemStream* _stream, const size_t data_size)
  {
      const size_t free_space = udp_io::availableForWrite();
      if (data_size > free_space)
      {
          //RTDB_Log(LOG_TYPE_INFO, "WriteToUdpSendBuffer: can't put cmd(%d) to udp send buffer(%d)! \n", data_size, free_space);
          Serial.printf("WriteToUdpSendBuffer: can't put cmd(%d) to udp send buffer(%d)! \n", data_size, free_space);
          return false;
      }

      size_t wrote_size = 0;
      if (_stream->end < _stream->pos)
      {
          wrote_size += udp_io::writeBytes(_stream->bufptr + _stream->pos, _stream->size - _stream->pos);
          if (wrote_size != 0)
              wrote_size += udp_io::writeBytes(_stream->bufptr + 0, _stream->end - 0);
      }
      else
          wrote_size += udp_io::writeBytes(_stream->bufptr + _stream->pos, data_size);

      _stream->pos = WRAP_IF_END(_stream, _stream->pos + data_size);

      if (wrote_size != data_size)
      {
          //SetRuntimeError(RTDB_TCP_WRITE_ERR);
          //RTDB_Log(LOG_TYPE_INFO, "Can't write data to udp, disconnect\n", data_size);
          udp_io::stop();
          return false;
      }

      //RTDB_Log(LOG_TYPE_INFO, "Write data to udp: %d bytes\n", data_size);
      return true;
  }
  FILTER_RESULT ServerEspCmdFilter(MemStream* stream, uint32_t start_pos, uint32_t messageId)
  {
      Serial.printf("Filter ESP cmd(%d)\n", messageId);
  //  if (ESP_SRV_RANGE_FIRST <= messageId && ESP_SRV_RANGE_LAST >= messageId) // this is esp command
  //  {
  //    //RTDB_Log(LOG_TYPE_INFO, "Filter ESP cmd(%d)\n", messageId);
		//
		//Serial.printf("Filter ESP cmd(%d)\n", messageId);
		//return PARSE_CMD;
  //  }
    
    stream->pos = start_pos;
    size_t data_size = GET_SIZE(stream);
    //RTDB_Log(LOG_TYPE_INFO, "Filter STM full cmd(%d): chunk=%d bytes\n", messageId, ssz);
	Serial.printf("Filter STM full cmd(%d): chunk=%d bytes\n", messageId, data_size);
    
    if (g_cmd_rec_clbk_ptr(stream))
    {
      return JUMP_TO_NEXT;
    }
    else
    {
      //RTDB_Log(LOG_TYPE_INFO, "UDP: Pause parsing!!!!\n");
	  //Serial.printf("UDP: Pause parsing!!!!\n");
      return PAUSE_PARSING;
    }
  }

  
  void EspConnectToAp(const char *_ssid, const char *_pass)
  {
	  WiFi.mode(WIFI_OFF);
	  delay(100);
	  WiFi.mode(WIFI_STA);
	  
	  Serial.printf("Connect to AP net:%s pas:%s\n", _ssid, _pass);
	  WiFi.begin(_ssid, _pass);
  }

  void OnServerDiscoveredClbk(const IPAddress& _ip_addr, const uint16_t _port, const uint32_t _server_id, const uint8_t _priority, const string _name)
  {
	  char serial[] = "12345";
	  string serial_str;
	  serial_str.ptr = serial;
	  serial_str.len = strlen(serial);
	  Serial.printf("ServerDiscovered: IP = %s, port = %d, server_id = %d, priority = %d, name = %d\n", _ip_addr.toString().c_str(), _port, _server_id, _name);
	  if (_port != 0) {
	
		Serial.printf("Found proper server");
		server_ip = _ip_addr;
		server_port = _port;

	  }
	  else {
		  if (server_port == 0) {
			  Serial.printf("NOT Found proper server ");
              if (WFM_GetPartalState()) {
                  serv_dis::askServersToAnnounce(1, serial_str, WFM_GetCustomIpAddres()); // Not Found Server
              }
              else
              {
                  serv_dis::askServersToAnnounce(1, serial_str); // Not Found Server
              }
			  
		  }
		  else {
			  // Connect to server
			  Serial.printf("Try connect to server\n\r");
			  connect(server_ip, server_port);
		  }
	  }
      



	  IPAddress server_ip;
	  uint16_t server_port;
	  uint32_t server_id;

  }


  void init(on_connected_clbk_ptr_t _conn_clbk_ptr, on_closed_clbk_ptr_t _clos_clbk_ptr, on_cmd_received_clbk_ptr_t _cmd_rec_clbk_ptr)
  {
    g_cmd_rec_clbk_ptr = _cmd_rec_clbk_ptr;
    g_conn_clbk = _conn_clbk_ptr;
    g_clos_clbk = _clos_clbk_ptr;
    
    InitEspProtoDescr(&esp_proto);
    esp_proto->Filter = ServerEspCmdFilter;
    // search for first free port from userspace ports
    while (1 != g_udp.begin(g_udp_port))
      ++g_udp_port;

    //RTDB_Log(LOG_TYPE_INFO, "Udp io service started at %d port\n", g_udp_port);
	Serial.printf("Udp io service started at %d port\n", g_udp_port);
    serv_dis::init(OnServerDiscoveredClbk);
    
    fsm::start();
	
  }

  void dispatchUpdateEvent()
  {
    UpdateEvent_T upd_event;
    upd_event.time = millis();
    fsm::dispatch(upd_event);
  }

  void dispatchIncomingUdpPackets()
  {
    int packet_size;
    while ((packet_size = g_udp.parsePacket()) > 0)
    {
      UdpIOPacket udp_packet = { 0 };
      if (udp_packet.readFrom(g_udp))
      {
        UdpIOPacketEvent_T up_event;
        up_event.remote_ip   = g_udp.remoteIP();
        up_event.remote_port = g_udp.remotePort();
        up_event.packet = &udp_packet;
        fsm::dispatch(up_event);
      }
    }
  }

  void update()
  {
    dispatchUpdateEvent();
    dispatchIncomingUdpPackets();

    if (!DispatchMessages(esp_proto))
    {
      //#TODO
      CBufClear(esp_proto->recvBuffer);
      //SetRuntimeError(RTDB_ESP_DISPATCH_MESSAGES_ERR);
      //RTDB_Log(LOG_TYPE_INFO, "!DispatchMessages(&esp_proto)\n");
	  //Serial.printf("!DispatchMessages(&esp_proto)\n");
    }
  }


  unsigned availableForWrite()
  {
    size_t free = CBufGetFreeSize(esp_proto->sendBuffer);
    return free > RESERVED_BUFF_SIZE ? free - RESERVED_BUFF_SIZE : 0;
  }


  unsigned writeBytes(const char *buffer, const size_t length)
  {
    return CBufWrite(esp_proto->sendBuffer, buffer, length);
  }


  bool isConnected()
  {
    return fsm::is_in_state<ConnStateEstablished_T>();
  }


  UdpIOStates_T status()
  {
    if (fsm::is_in_state<ConnStateEstablished_T>())
      return UIO_ST_ESTABL;

    if (fsm::is_in_state<ConnStateClosed_T>())
      return UIO_ST_CLOSED;

    return UIO_ST_STAGE1;
  }


  void stop()
  {
    EspConnectToTcpEvent_T event;
    event.ip_addr = IPAddress();
    event.port    = 0;
    fsm::dispatch(event);
  }


  void connect(const IPAddress &_ip_addr, const uint16_t _port)
  {
    //RTDB_Log(LOG_TYPE_INFO, "Connect to server ip:%s : %d\n", _ip_addr.toString().c_str(), _port);
	Serial.printf("Connect to server ip:%s : %d\n", _ip_addr.toString().c_str(), _port);
	//LogBuff_AddEvent(FIND_SERVER, 2);
	EspConnectToTcpEvent_T event;
    event.ip_addr = _ip_addr;
    event.port    = _port;
    fsm::dispatch(event);


	// Send log
  }

} // namespace udp_io

FSM_INITIAL_STATE(udp_io::UdpIOFSM_T, udp_io::ConnStateClosed_T)