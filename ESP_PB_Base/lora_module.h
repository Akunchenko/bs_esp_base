#ifndef _LORA_MODULE_h
#define _LORA_MODULE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include <LoRa.h>
#include <SPI.h>

void InitLora();
	void sendMessage(String outgoing);
	void onReceive(int packetSize);
	uint32_t GetLoRaClientAdress();
	void SetLoRaClientAdress(uint32_t _sender);
	void sendACK(uint8_t _target, uint32_t _buf);
#endif

