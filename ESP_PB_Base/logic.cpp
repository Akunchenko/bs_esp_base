#include "lora_module.h"
#include "logic.h"
#include "Periphery.h"
#include "wifi_module.h"
#include "lora_module.h"
#include "Indication.h"
#include "callbacs.h"

#include <power_control\src\power_control.h>
#include <Lan_proto/callbacs.h>
//Periphery outward;
bool afs;
uint16_t adc;
MyLogic::MyLogic() {
	Periphery();
	Indication();
	
	InitLora();
	sysState = INIT_POWER;//INIT_PEREPHERY
	Serial.println("INIT_PEREPHERY");
	outward.SetDcDcState(HIGH);
}

void MyLogic::Handler() {
	
	if (sysState == INIT_PEREPHERY) {
		if (outward.GetPowerButtonState() && millis() > 1000) {
			sysState = INIT_POWER;
			Serial.println("INIT_POWER");
			outward.SetDcDcState(HIGH);
			
		}

	}
	if (sysState == INIT_POWER) {
		InitWiFi();
		sysState = INIT_WIFI;
		Serial.println("INIT_WIFI");
	}
	if (sysState == INIT_WIFI) {
		outward.ControlDisablingBoard();
		HandleWiFi();
		onReceive(LoRa.parsePacket());
	}
	indication.Handler();
	
	yield();
	//if (!outward.GetChargeState()) {
	//	//indication.SetChargeWsColor(Indication::WS_COLORS::WS_RED_COLOR);
	//}
	//else
	//{
	//	//indication.SetChargeWsColor(Indication::WS_COLORS::WS_GREEN_COLOR);
	//}
	//if (GetLoRaClientAdress() == 0xbb) {
	//	SetLoRaClientAdress(0);
	//	outward.SetBuzzerState(HIGH);
	//	PinpointBuzzerTime(200);
	//}
	//if (millis() >= _timerOffBuzzer) {
	//	if (afs) {
	//		afs = false;
	//	}
	//	else
	//	{
	//		afs = true;
	//	}
	//	/*adc = analogRead(A0);
	//	Serial.print("ADC = ");
	//	Serial.println(adc);*/
		
		
	//	PinpointBuzzerTime(5000);
	//}
}

void MyLogic::PinpointBuzzerTime(uint32_t time) {
	_timerOffBuzzer = millis() + time;
}

