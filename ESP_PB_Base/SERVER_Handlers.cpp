
#include <ESP8266WebServer.h>
#include "SERVER_FS_Handlers.h"
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include "SettingsData.h"
#include <new>
ESP8266WebServer server(80);
SettingsData** settingsArr;
// ���������� ���������� wifi
String _ssid = "Home_Production_Guest"; // ��� �������� SSID
String _password = "12345678"; // ��� �������� ������ ����
String _ssidAP = "WiFi";   // SSID AP ����� �������
String _passwordAP = ""; // ������ ����� �������
String SSDP_Name = "SendToWeb"; // ��� SSDP
int timezone = 3;               // ������� ���� GTM

void handleRoot() {
    handleFileRead("/");
}
void handleNotFound() {
	String message = "File Not Found\n\n";
	message += "URI: ";
	message += server.uri();
	message += "\nMethod: ";
	message += (server.method() == HTTP_GET) ? "GET" : "POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";
	for (uint8_t i = 0; i < server.args(); i++) {
		message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
	}
	server.send(404, "text/plain", message);
}
void handle_ConfigJSON() {
    String json = "{";  // ����������� ������ ��� �������� � ������� json ������
    //{"SSDP":"SSDP-test","ssid":"home","password":"i12345678","ssidAP":"WiFi","passwordAP":"","ip":"192.168.0.101"}
    // ��� SSDP
    json += "\"SSDP\":\"";
    json += SSDP_Name;
    // ��� ����
    json += "\",\"ssid\":\"";
    json += _ssid;
    // ������ ����
    json += "\",\"password\":\"";
    json += _password;
    // ��� ����� �������
    json += "\",\"ssidAP\":\"";
    json += _ssidAP;
    // ������ ����� �������
    json += "\",\"passwordAP\":\"";
    json += _passwordAP;
    // ��������� ����
    json += "\",\"timezone\":\"";
    json += timezone;
    // IP ����������
    json += "\",\"ip\":\"";
    json += WiFi.localIP().toString();
    json += "\"}";
    server.send(200, "text/json", json);
}
void InitServerHandlers(void) {
    server.on("/", handleRoot);

    server.on("/configs.json", handle_ConfigJSON);
    server.on("/config/set", HTTP_POST, []() {
        String payload;
        uint8_t count=0;
        String mess;
        for (uint16_t i = 0; i < server.args(); i++)
        {
            payload += server.arg(i);
            
        }
        //Serial.println(payload);
        //Serial.print(payload.length());
        /*for (uint8_t i = 1; i <= 10; i++)
        {
            mess = "";
            mess += "settings";
            mess += i;
            for (int j = 0; j < payload.length(); j++)
            {
                String res = payload.substring(j, j + mess.length());
                if (res == mess) {
                    count++;
                    Serial.print("Count: ");
                    Serial.println(count);
                }

            }
        }*/
        const size_t capacity = 9 * JSON_ARRAY_SIZE(4) + JSON_OBJECT_SIZE(10) + JSON_OBJECT_SIZE(12) + 9 * JSON_OBJECT_SIZE(13) + 2200;
        DynamicJsonDocument doc(capacity);
        deserializeJson(doc, server.arg("plain"));

        settingsArr = new SettingsData* [doc.size()];
        
        for (uint8_t i = 0; i < doc.size(); i++)
        {
            String mess = "";
            mess += "settings";
            mess += i+1;
            JsonObject settings = doc[mess];

            settingsArr[i] = new SettingsData();
            settingsArr[i]->setGameTimeMin(settings["gameTimeMin"]);
            settingsArr[i]->setGameTimeSec(settings["gameTimeSec"]);
            settingsArr[i]->setConditionToWin(settings["conditionToWin"]);
            settingsArr[i]->setSwitchSideEach(settings["switchSideEach"]);
            settingsArr[i]->setTimeOutMin(settings["timeOutMin"]);
            settingsArr[i]->setTimeOutSec(settings["timeOutSec"]);
            settingsArr[i]->setOverTimeMin(settings["overTimeMin"]);
            settingsArr[i]->setOverTimeSec(settings["overTimeSec"]);
            settingsArr[i]->setBreakTimeMin(settings["breakTimeMin"]);
            settingsArr[i]->setBreakTimeSec(settings["breakTimeSec"]);
            settingsArr[i]->setTypeOfGame(settings["teamGame"]);
            JsonArray settings_teamNames = settings["teamNames"];
            byte countElem = settings_teamNames.size();
            Serial.print("countElem");
            Serial.println(countElem);
            settingsArr[i]->createBufTeamNames(countElem);
            for (uint8_t j = 0; j < countElem; j++)
            {
                settingsArr[i]->setTeamNames(settings_teamNames[j], j);// "TeamA"
            }
            Serial.println("teamNames: ");
            for (uint8_t j = 0; j < countElem; j++)
            {
                Serial.println(settingsArr[i]->getTeamNames(j));
            }
        }
       /* Serial.print(F("Sending: "));
        serializeJson(newBuffer, Serial);
        Serial.println();*/
        // ���������� �����
        server.send(200, "application/json", "{Succses}");
    });
    
}
void Server_Init() {
    FS_init(server);
    InitServerHandlers();
    server.begin();
}
void ServerHandleClient(){
	server.handleClient();
}
